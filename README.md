
thesenicolibre
==============

« Libre et Santé. Comprendre le logiciel libre et ses enjeux pour les
professions de santé. » est une thèse de doctorat en pharmacie,
soutenue à Lille (France) le 2 avril 2014, par Nicolas Floquet.

- Les fichiers présents dans le répertoire "redaction" constituent
  les sources de la thèse.

- Les fichiers présents dans le répertoire "soutenance" constituent
  les sources du diaporama de la soutenance.

Copyright (C) Nicolas Floquet, License CC-BY-SA (voir le fichier
COPYING.txt)


