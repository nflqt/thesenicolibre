
\chapter{1944-1987, l'informatique}
\label{chap:info}

\parbox{0.6\textwidth}{{\sffamily {\itshape Seul le logiciel libre
    permet l'éducation à l'informatique.}} \cite{Gnu}

\raggedleft Richard \textsc{Stallman}} \vspace{4ex}

Nous débuterons cette étude du Libre par un chapitre qui semblera ne
traiter que d'ordinateur, ou de réseau. Mais il s'agit bien là de la
naissance du libre. Il traite du développement de la science
informatique jusqu'à la fin des années 50 puis de la diffusion de la
culture hacker parmi les informaticiens des années 60 et 70.

Nous pouvons supposer que, depuis très longtemps, l'Homme utilise des
outils --- tels que les bouliers --- pour l'aider à effectuer des
calculs. Puis :

\begin{itemize}
\item En 1642, Blaise \textsc{Pascal} invente une machine à calculer,
  capable d'effectuer des additions et des soustractions.
\item En 1801, Joseph Marie \textsc{Jacquart} perfectionne les métiers
  à tisser programmables à cartes perforées. Ce type de carte sera le
  support privilégié des programmes durant un siècle et demi.
\item En 1834, Charles \textsc{Babbage} rassemble les deux concepts
  précédents en inventant la «machine analytique», une machine à
  calculer programmable avec les cartes perforées du métier à tisser
  Jacquart. Voici l'ancêtre des ordinateurs et il fonctionnait à la
  vapeur.
\item À la fin du XIX\ieme siècle, les travaux d'Herman
  \textsc{Hollerith}\footnote{Herman \textsc{Hollerich} est aussi le
    fondateur d'IBM, \emph{International Bussiness Machines
      Corporation}}, ingénieur américain, aboutissent à la
  mécanographie : l'ensemble de techniques mécaniques (puis
  électro-mécaniques) de traitement de l'information par des machines
  à cartes perforées et à lampes radio\footnote{La chaleur dégagée par
    ses lampes attirait les insectes (en anglais : bugs), ce qui
    constituait une cause de panne courante. Ce n'est plus le cas sur
    nos ordinateurs actuels, mais c'est ainsi que l'expression
    désignant une erreur de programmation est née.}.
\end{itemize}

Ces machines à carte perforées et à lampes radio sont alors utilisées
jusqu'à la seconde guerre mondiale. C'est autour des dates de ce
conflit qu'on peut situer la période de naissance de l'informatique
moderne.

\section{Une nouvelle science}

\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape L'accès aux codes source
      [des] programmes constitue la seule vraie garantie de pérennité
      et d’indépendance des usagers vis-à-vis des éditeurs.}}
  \cite{Cos98b}

  \raggedleft Roberto \textsc{Di Cosmo}} \vspace{2ex}

\subsection{Les premiers ordinateurs}

Trouver une date exacte à l'apparition du premier ordinateur est un
sujet controversé. Cependant, des ordinateurs tels que nous les
connaissons sont issus d'inventions et de découvertes successives
telles que les suivantes :

\begin{itemize}

\item En 1944, l'IBM ASCC (Automatic Sequence Controlled Calculator)
  --- ou Mark I --- est le premier calculateur entièrement automatique,
  c'est-à-dire, n'ayant pas besoin d'intervention humaine.

\item En 1945, l'architecture de type \textsc{Von Neumann} ---
  aujourd'hui utilisée par toutes les machines que nous utilisons
  quotidiennement --- distingue différentes parties pour un ordinateur
  moderne. La particularité de cette architecture est de ne plus
  séparer physiquement le stockage des données et celui des
  programmes. Ainsi, nos disques durs actuels contiennent aussi bien
  des données que des programmes. Cela semble aller de soi pour un
  utilisateur contemporain ; mais ce n'est pas le cas.\footnote{Plus
    précisément, l’architecture de \textsc{von Neumann} décompose
    l’ordinateur en 4 parties :
\begin{itemize}
\item l’unité de traitement qui effectue les calculs et les
  résolutions d'algorithme ;
\item l’unité de contrôle, chargée du séquençage des opérations ;
\item la mémoire qui contient à la fois les données requises, les
  instructions (programmes) et les données générées ;
\item les dispositifs d’entrée-sortie, les interfaces entre
  l'ordinateur et l'extérieur.
\end{itemize}}

\item En 1950, sont construits les premiers ordinateurs utilisant le
  concept de \emph{machine universelle de Turing}.\footnote{Alan
    \textsc{Turing} (1912-1952), scientifique britannique, décrit le
    test de Turing en 1950 dans sa publication \emph{Computing
      mchinery and intelligence}, œuvre importante dans le domaine de
    l'intelligence artificielle dans laquelle Turing tente de répondre
    à la question : «Une machine peut-elle penser ?»} Ceux-ci peuvent
  désormais effectuer la résolution d'\emph{algorithmes}\footnote{Un
    algorithme est une suite d'instructions permettant de résoudre un
    problème complèxe ; il ne s'agit plus de simples calculs.} en plus
  des simples calculs.

\item Grace \textsc{Hopper}

\item Durant les années 60 et 70, grâce à de nombreux
  travaux\footnote{Les interfaces graphiques modernes sont issues des
    recherches en ergonomie et en psychologie cognitive menées par le
    centre de recherche de la compagnie Xerox.}, nous pouvons voir
  apparaître sur un périphérique \emph{de sortie} appelé \emph{écran}, une
  métaphore de bureau (le \emph{desktop}) équipé de tiroirs (les
  \emph{menus}), à partir duquel nous pouvons éxécuter puis utiliser
  nos programmes à l'aide d'un périphérique \emph{d'entrée} appelé
  \emph{souris} \footnote{La souris a été inventée par Douglas
    \textsc{Engelbart} de Stanford dans les années 60.}, dont les
  déplacement sont répercutés sur le \emph{curseur} visible à l'écran.

\item Enfin, tout au long de ces décennies, les inventions au niveau
  \emph{hardware}\footnote{Le \emph{hardware} est constitué par le
    \emph{matériel} informatique (les appareils, les machines, les
    périphériques d'entrée et de sortie...) par opposition au
    \emph{software} : les programmes aussi appelés \emph{logiciels}
    (scripts, bibliothèques, applications...). Cependant, à l'origine,
    le logiciel était également de nature matérielle : les premiers
    logiciels consistaient en des configurations données
    d'interrupteurs et de cables sur les premiers
    ordinateurs. \cite{Mog01}} --- comme le transistor, puis le
  circuit imprimé --- ont considérablement augmenté la puissance de
  calcul des ordinateurs tout en diminuant leur taille.
\end{itemize}

\subsection{L'informatique}

Les inventions qui se sont succédées durant ces décennies ont donné
naissance à une toute nouvelle branche de la science et des techniques
: \emph{l'informatique}.

L'informatique \footnote{Informatique est un mot-valise issu de la
  fusion des mots \emph{information} et \emph{automatique}. Il a été
  inventé par l'ingénieur allemand Karl Steinbuch. Il a été introduit
  dans la langue française par l'ingénieur Philippe Drefus, puis
  reconnu par l'académie française en 1966.} est avant tout la science
qui étudie le traitement automatique de l'information.

Mais la science informatique est aussi la base de nombreuses
applications industrielles et commerciales qu'on regroupe également
sous le terme d'informatique dans la langue française, ce qui nuit
peut-être à la compréhension\footnote{L'anglais distingue la science
  \emph{computer science} de ses applications \emph{computing}.}.

Le mot \emph{informatique} désigne donc une branche de la science avec
ses applications perceptibles par chacun.

\subsubsection{Programmation et code source}

Les premiers programmes étaient inscrits sur des cartes perforées,
puis avec des tubes à vide, et enfin avec des transistors. Mais ces
supports ne suffirent plus à inscrire les programmes dont la taille et
la complexité ne cessaient de croitre. De plus, il devint pénible,
voire impossible de coder de tels programmes en language binaire,
c'est-à-dire avec uniquement deux symboles : le zéro et le un.

Alors des \emph{languages de programmation} furent inventés : leur
principe est de permettre aux programmeurs de coder plus facilement et
plus rapidement. Une étape de traduction devenant nécessaire pour
créer le véritable programme en language binaire. Cette étape est
automatisée et fait donc elle-même l'objet d'un type particulier de
programme effectuant cette traduction. Cette étape s'appelle
\emph{compilation} et nécessite de lancer un \emph{compilateur} qui se
charge de traduire le code en language de programmation, également
appelé \emph{code source}, en code \emph{binaire} fonctionnel,
également appelé \emph{exécutable}. \footnote{Il existe également un
  second type de langage informatique : les languages
  \emph{interprétés} pour lequels la traduction du code source en code
  exécutable s'effectue à la volée à chaque exécution du
  programme. L'étape de compilation n'existe pas, ou plutôt elle est
  simultanée à l'exécution du programme.}

\begin{figure}
\centering
\rule{0.8\textwidth}{1pt}\vspace{2ex}
\includegraphics[width=0.8\textwidth]{compilation}
\caption[Processus de compilation]{Compilation}
\rule{0.8\textwidth}{1pt}
\end{figure}

Nous pouvons comparer ce processus à la cuisine. C'est en effet une
analogie très courante que celle de la recette de cuisine et du plat
résultant. Le code source devient ainsi une recette décrivant
parfaitement chaque ingrédient et chaque étape de préparation, tandis
que le code binaire est le plat, c'est-à-dire un résultat utilisable
--- mangeable dans ce cas --- mais à partir duquel il est extrémement
difficile de connaître l'ensemble des ingrédients mais aussi la
manière de les préparer en vue de refaire le plat chez
soi.\footnote{Comme Vincent \textsc{Lozano} \cite{Loz10} ou Richard
  \textsc{Stallman} \cite{Gnu}, nous avons choisi l'analogie avec la
  cuisine. Laurent \textsc{Chemla} \cite{Che02} lui utilise l'analogie
  de la peinture : ainsi si un peintre fournissait la liste des
  teintes utilisées et de la totalité des gestes effectués, il ferait
  de la \emph{peinture libre}.}

Consulter le code source est donc l'unique moyen de découvrir
précisément la façon dont fonctionne un programme. En effet, de même
qu'il est très long et difficile de réécrire la recette rien qu'en
goûtant le plat, il est très long et difficile de reconvertir un
binaire en source\footnote{Une étape de conversion d'un binaire en
  source est appelée \emph{reverse-engineering} ou
  \emph{rétroingénierie}, ou encore \emph{rétroconception}.}. Par
conséquent, ne pas diffuser le code source constitue le meilleur moyen
de conserver le secret sur le fonctionnement de son programme.

\subsubsection{Systèmes d'exploitation}

Dans les années 50, les ordinateurs étaient d'énormes machines très
coûteuses. Les universités et les grosses entreprises n'en possédaient
que rarement plus d'un. Avec les besoins croissants en ressources
informatiques de tous les scientifiques de ces sites industriels ou
universitaires, il était nécessaire de rendre plus efficace
l'utilisation de ces rares et chers ordinateurs.

Insérer les programmes dans la machine et la lancer était une tâche
fastidieuse, et très protocolaire qui nécessitait le travail d'un
technicien chargé de rassembler les cartes perforées en lot. Cela
agaçait les chercheurs qui devaient patienter longtemps pour voir
leurs requêtes exécutés et recevoir leurs résultats. \cite{Sta10}

Pour automatiser ces tâches répétitives, pour diminuer les
interventions manuelles, les informaticiens ont ainsi créé des
programmes servant à lancer d'autres programmes. En d'autre termes,
ils ont créer des ensembles de programmes, dits \emph{systèmes},
servant à mieux \emph{exploiter} les possibilités des ordinateurs. Ils
ont donc créé des \emph{systèmes d'exploitation}.

Les systèmes d'exploitation évolué par les ajouts successifs de trois
principes suivant :
\begin{enumerate}
\item Le \emph{traitement par lot}. Dans les années 50, les systèmes
  d'exploitation fonctionnaient très simplement selon le principe du
  \emph{traitement par lot} ou \emph{batch processing}. Les
  différentes commandes des utilisateurs sont rassemblées par lot par
  des techniciens, et l'ordinateur les traite les unes après les
  autres. Quand celui-ci a terminé, l'ordinateur entamme le lot de
  requêtes suivant.
\item La \emph{multi-programmation}. C'est dans les années 60, afin
  d'optimiser l'utilisation du matériel, qu'est apparu l'idée de faire
  exécuter par la machine plusieurs programmes \emph{en même
    temps}. Grâce à ce principe, les systèmes d'exploitation
  deviennent \emph{multi-tâches}.
\item Le \emph{temps partagé}. Enfin à la fin des années 60, afin de
  permettre aux scientifiques d'utiliser directement les ressources
  des ordinateurs sans passer par un service technique centralisant
  les requêtes, le principe du \emph{temps partagés} ou \emph{time
    sharing} a permis à plusieurs utilisateurs de partager l'accès à
  un même ordinateur via des \emph{terminaux} --- des ensembles
  constitué d'un clavier et d'un écran --- qui était réparti sur tout
  un site donné. Grâce à ce principe, les systèmes d'exploitation
  deviennent \emph{multi-utilisateurs}.
\end{enumerate}

D'autres principes de systèmes d'exploitations furent conçus
depuis. Cependant, ceux-ci ne sont utilisés que pour des applications
scientifiques poussées. Les systèmes d'exploitation que nous utilisons
couramment de nos jours se cantonnent aux trois principes de
fonctionnement vus précédemment.

Enfin, il nous semble important d'insister sur le fait que ses
principes se sont ajoutés les uns sur les autres, empilant les couches
d'abstraction théoriques. Le principe du traitement par lot est donc
toujours présent dans nos systèmes d'exploitation \emph{modernes}.

\subsubsection{Plates-formes, portabilité et implémentation}

La \emph{plateforme} constitue le contexte informatique au sein
duquel un programme s'exécute, il s'agit du matériel d'une part et de
l'environnement logiciel d'autre part.\footnote{Ceci dans le cas d'un
  langage \emph{compilé}. Pour un language de type \emph{interprété},
  la logique est basiquement identique mais la \emph{plate-forme} est
  constituée par le couple interpréteur/machine.}
\begin{itemize}
\item En effet, un ordinateur ne fonctionne qu'à partir d'un jeu
  d'instruction basiques en langage binaire. Or un jeu d'instruction
  est propre à une \emph{architecture}. Autrement dit, le code binaire
  d'un programme donné n'est pas le même selon l'ordinateur qui doit
  l'executer. 
\item De même, un système d'exploitation est constitué de programmes
  de bas niveau et de \emph{bibliothèques}\footnote{Une
    \emph{bibliothèque} est une collection de fonctions prêtes à être
    utilisées telle quelle par des programmes. Leur existence évite
    aux programmeurs d'avoir à créer des pans entiers de programmes
    alors que ceux-ci existent déjà. On les appelle également
    \emph{librairie} mais il s'agit d'une traduction impropre de
    l'anglais \emph{library}.} qui lui sont propres. Un binaire
  compilé pour une machine et un système d'exploitation donné n'est
  donc pas exécutable sur une machine identique mais fonctionnant à
  l'aide d'un autre système d'exploitation.
\end{itemize}

C'est le \emph{compilateur} --- type de programme essentiel que nous
avons découvert précédemment --- qui se charge de traduire de façon
adéquate un programme source pour une machine et un système
d'exploitation donné. \cite{Loz10}

\begin{figure}
\centering
\rule{0.8\textwidth}{1pt}\vspace{2ex}
\includegraphics[width=0.8\textwidth]{portabilite}
\caption[Notion de portabilité]{Portabilité. Un langage de
  programmation portable permet de compiler un code source pour de
  nombreux ensembles machine/système d'exploitation différents.}
\rule{0.8\textwidth}{1pt}
\end{figure}

La \emph{portabilité} d'un programme est sa capacité à pouvoir être
adapté facilement en vue de son exécution d'une plateforme à une
autre. Enfin, on appelle \emph{portage} l'action de modifier un
programme pour qu'il puisse s'exécuter sur une plateforme différente
que celle pour laquelle il a été conçu initialement.

Les premiers systèmes d'exploitation étaient construits pour un seul
type de machine. Le fait pour un système d'exploitation complet d'être
portable, lui conférait l'avantage essentiel par rapport à des
systèmes d'exploitation pas ou peu portable d'être facilement adapté
pour l'installation sur des ordinateurs de marque différente.

Dans les cas où le portage d'un programme n'est pas réalisable, on va
l'\emph{implémenter} \footnote{Le mot implémentation est également un
  anglicisme.}, c'est-à-dire coder entièrement un nouveau programme
avec le langage adapté à la nouvelle machine, de manière à ce qu'il
soit semblable au programme originel. Il s'agit bien d'un programme
différent, son code source n'a aucun rapport avec celui d'origine,
pourtant un utilisateur pourrait croire avoir affaire au même
logiciel.


\section{Une nouvelle culture}

\parbox{0.6\textwidth}{{\sffamily {\itshape Pour moi la programmation
      est plus qu'un art appliqué important. C'est aussi une
      ambitieuse quête menée dans les tréfonds de la connaissance.}}

  \raggedleft Chris \textsc{Di Bona}} \vspace{2ex}

Nous avons beaucoup discuté des programmes, mais encore peu des
personnes qui en sont à l'origine.  Dans les années 60, la plupart des
ordinateurs étaient d'énormes machines dont seules les universités et
les très grandes entreprises avaient les moyens de profiter. Ce type
d'ordinateur se constitue d'une unité centrale à laquelle se relie de
nombreux --- jusqu'à des centaines --- terminaux, c'est-à-dire guère
plus que de simples ensembles clavier-écran. \footnote{Aujourd'hui,
  les super-ordinateurs conçus pour être utilisés de cette manière
  sont appelés \emph{mainframe}.} Et les utilisateurs des ordinateurs
étaient donc des chercheurs, des ingénieurs, des techniciens et des
étudiants, qui se partageaient l'utilisation des terminaux. Parmi eux,
nombreux étaient ceux qui --- non contents de lancer des requêtes aux
ordinateurs et de profiter des programmes préexistants ---
contribuaient collectivement à améliorer les programmes de
l'ordinateur. Ces personnes sont les \emph{hackers}.

\subsection{Hackers}

Hackers est un terme de jargon informatique. Il désigne un
informaticien particulièrement compétent, ou --- au sens large --- une
personne qui apprécie réellement mettre en œuvre toute son astuce et
toute son intelligence dans la programmation des machines.

\subsubsection{Des étudiants aux informaticiens}

On remonte l'origine du terme hacker aux années cinquante. C'était
ainsi que se désignaient eux-mêmes certains étudiants du
MIT\footnote{Massachusets Institut of Technologie, prestigieuse
  université située à Cambridge, près de Boston, au nord-est des
  États-Unis.} qui passaient leur temps au \emph{hacking}\footnote{Le
  sens premier du verbe \emph{to hack} est \emph{tailler} ou
  \emph{taillader}. Ces étudiants du MIT utilisaient la métaphore de
  se fraie un chemin à la machette pour désigner la recherche
  d'astuces pour parvenir à réaliser leurs projets de loisirs.} les
ordinateurs et le réseau téléphonique de leur université --- en dehors
des créneaux horaires d'utilisation, donc souvent la nuit. Les
informaticiens attachés au MIT étaient extrémement renommés dans les
années soixante et soixante-dix. Beaucoup étaient eux-même d'anciens
étudiants du MIT. C'est ainsi que le terme à fini par s'appliquer aux
informaticiens de ce site universitaire. \cite{Sta10}

Puis, dans les années 70, ARPAnet, l'ancêtre d'internet, s'est
développé en connectant les ordinateurs de sites militaires,
universitaires ou industriels, toujours plus nombreux. Ce réseau a
multiplié les possibilités d'échange d'information entre les
programmeurs de tous les États-Unis. La culture hacker s'est diffusée
et enrichie. Et d'autres informaticiens présents sur d'autres sites
que celui du MIT ont eux-aussi pu acquérir le titre --- officieux mais
extrèmement gratifiant dans le milieu des informaticiens--- de Hacker.

Eric S. \textsc{Raymond}, un hacker de renommée internationale,
utilise le terme de \emph{nation réseau} pour dénommer l'ensemble des
hackers de cette époque. Il explique que la DARPA laissait
volontairement les utilisateurs d'ARPAnet s'adonner à des activités
sociales annexes --- telles que la discussion autour des œuvres de
science-fiction --- pourtant non-autorisées par le réglement, car
c'était une manière d'attirer des chercheurs et étudiants brillants
vers l'informatique. \cite{Dib99}

C'est ainsi qu'une culture \emph{informaticienne} s'est développée et
les héros de cette culture sont les \emph{hackers}.

\subsubsection{Une éthique informaticienne}

Un hacker est donc une personne dont l'habileté à la manipulation de
l'abstraction des programmes informatiques n'a d'égale que son mépris
pour les règles inutiles qui lui font perdre du temps. \cite{Sta10}
Eric S. \textsc{Raymond} décrit les hackers autant comme des
informaticiens brillants que comme des militants ayant une éthique
forte.  Selon lui, les hackers croient : \cite{Ray01}
\begin{itemize}
\item en la liberté ainsi qu'en l'assistance mutuelle,
\item que le monde est plein de problèmes fascinants en attente de
  solutions,
\item que les corvées sont de mauvaises choses, et qu'on ne devrait
  jamais résoudre deux fois un même problème,  
\item que, quoi qu'il arrive, l'état d'esprit ne remplace pas la
  compétence.
\end{itemize}

L'informatique est ainsi le socle d'un nouveau positivisme. Seulement,
au grand dam des véritables hackers, les médias occultent souvent cet
aspect. Ils réduisent l'informatique à du gadget ; et n'utilisent le
terme de hacker que pour désigner des délinquants n'ayant pour seul
objet que la destruction et dont le génie supposé se borne finalement
au recopiage de programmes de sabotage. Les hackers véritables
appellent ces personnes des \emph{crackers} et \textsc{Raymond} résume
très simplement :
\begin{quotation}
  \sffamily \itshape Les hackers construisent ce que les crackers
  détruisent. \cite{Ray01}
\end{quotation}

\subsection{ARPAnet}

Nous venons de mentionner ARPAnet, qui est considéré comme le
prédécesseur d'internet car il partage les mêmes principes fondateurs.

\subsubsection{Les réseaux informatiques}

Un réseau informatique est un groupe d'ordinateur capables d'échanger
des informations entre eux. On oublie généralement que le mot
\emph{réseau} est à l'origine un synonyme de \emph{filet} \footnote{Un
  réseau est un \emph{petit rets} soit un filet aux mailles serrées.},
c'est cependant la raison pour laquelle les extrémités et
intersections des connections (un ordinateur ou un autre équipement
informatique) sont appelées des \emph{nœuds} comme dans le cas du
filet.

Le besoin d'échange d'informations entre ordinateurs s'est très vite
sentie dans l'histoire de l'informatique.

\subsubsection{La commutation de paquet}

C'est en 1961, que Leonard Kleinrock, alors étudiant au MIT, publia un
article concernant le transfert de données \emph{par paquet}. La
\emph{commutation de paquet} ou \emph{packet switching} se fonde sur
plusieurs principes :
\begin{itemize}
\item le découpage des données, afin d'en accélérer le transfert,
\item l'étiquettage de chaque bout de données par un en-tête contenant
  des information sur la destination des données, l'ensemble formant
  un \emph{paquet} par analogie avec le transport physique des objets.
\item l'aiguillage du paquet vers sa destination par un commutateur ;
  un \emph{commutateur} ou \emph{switch} étant un matériel
  informatique qui permet de créer des circuits virtuels.
\end{itemize}

On oppose la commutation de paquet à la commutation de circuit,
principe sur lequel se fondent les premiers réseaux de
télécommunication. Le principe de la commutation de paquet est plus
complexe mais plus souple, et il utilise généralement mieux le
potentiel du support physique de réseau. Il s'agit là de l'une des
raisons pour lesquelles Internet --- réseau à commutation de paquet
--- a finit par s'imposer à grande échelle contre des réseaux à
commutation tel que le Minitel qui reposait sur la commutation de
circuit. \cite{Bay10}

\subsubsection{Un réseau acentré}

C'est en 1969, que la DARPA \footnote{La DARPA (Defense Advanced
  Research Project Agency) de l'armée des États-Unis, agence créée en
  1958, est le prescripteur de nombreux projets informatiques dont
  certains sont très fameux :
  \begin{itemize}
  \item Multics, système d'exploitation à l'origine d'UNIX, Ken
    \textsc{Thompson} et Dennis \textsc{Ritchie} ayant participé à ce
    projet peu avant de développer de leur côté certaines idées de
    Multics ; nommant leur travail \emph{Unics} en référence.
  \item ARPAnet, réseau à l'origine d'Internet,
  \item GPS (Global Positioning System), système de localisation par
    satellite accessible au grand public.
  \end{itemize}} --- agence de recherche de l'armée des États-Unis
d'Amérique --- lance le projet ARPAnet (Advanced Research Projects
Agency Network)

Les objectifs de la DARPA étaient notamment :
\begin{itemize}
\item de faire baisser les coûts de recherche par le partage des
  données et des ressources entre les différents sites de recherche,
  et d'éliminer les projets doublons,
\item de pouvoir continuer à communiquer malgré d'importantes
  destructions par action militaire.
\end{itemize}

C'est ce dernier objectif qui a fait aboutir les concepteurs à l'idée
d'un réseau qui n'a pas de centre, un réseau \emph{acentré}. En effet,
un réseau sans centre peut continuer à fonctionner malgré
l'inactivation ou la destruction de certains de ses nœuds. \cite{Bay10}

De deux nœuds lors de son lancement --- les universités de Stanford et
de Los Angeles, toutes deux en Californie --- l'expansion d'ARPAnet
atteint vingt-trois nœuds en 1971, Cent-onze nœuds en 1977 etc. Tous
les centres de recherche majeurs des États-Unis furent
interconnectés. \cite{Dib99}

Ainsi, ARPAnet est doublement l'ancêtre d'internet :
\begin{itemize}
\item pour des raisons techniques, puisque leur principe de
  fonctionnement est similaire.
\item pour des raisons sociologiques parce qu'il est à l'origine des
  premières \emph{communautés} de hackers, communautés qui perdurent
  sur Internet.
\end{itemize}

\subsection{UNIX}

UNIX est un système d'exploitation d'ordinateur dont la conception a
débuté à la fin des années 60. Il a donné naissance à une famille
entière de systèmes d'exploitation très utilisés aujourd'hui, dont :
\begin{itemize}
\item BSD, une famille de systèmes d'exploitation libres très utilisée
  pour les serveurs. Une part importante des serveurs internet tourne
  d'ailleurs sous BSD.
\item OS X, pour les ordinateurs de bureau produits par Apple,
\item iOS, pour les smartphones et tablettes tactiles Apple,
\item Android, le système d'exploitation pour smartphone développé par
  Google,
\item et bien sûr GNU/Linux, que nous étudierons en détail dans le
  \emph{chapitre 2}.
\end{itemize}

\subsubsection{Un système d'exploitation portable}

L'histoire d'UNIX commence en 1969, la même année que pour
ARPAnet. Ken \textsc{Thompson} et Dennis \textsc{Ritchie} des
laboratoires Bell Labs d'AT\&T \footnote{Les laboratoires Bell Labs
  constituaient le département de recherche et de développement de la
  société américaine de téléphonie AT\&T \emph{American Telephone \&
    Telegraph}.} se lancent dans la création d'un nouveau système
  d'exploitation présentant les caractéristiques suivantes :
\begin{itemize}
\item à temps partagé donc multi-utilisateur,
\item et portable donc multi-plateforme.
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{kenthompsondennisritchie-peterhamer}
\caption[Ken \textsc{Thompson} et Dennis \textsc{Ritchie}]{Ken
  \textsc{Thompson} (assis), Dennis \textsc{Ritchie} et un PDP-11}
\footnotemark
\label{fig:thomsonritchie}
\end{figure}

\footnotetext{L'auteur de la photographie présentée en figure
  \ref{fig:thomsonritchie} page \pageref{fig:thomsonritchie} est Peter
  Hamer, CC-BY-SA \cite{Wikipedia}}

Les qualités techniques d'UNIX lui valut d'être installé dans les
années 70 sur de nombreux sites tout autour du monde. En 1973, UNIX se
retrouvait déjà sur 17 sites. En 1974, UNIX est distribué gratuitement
aux universités. À terme, ce sont des centaines de sites qui
utilisèrent UNIX \cite{Loz10}. On retrouvait UNIX plus souvent encore
qu'\emph{ITS} \footnote{Incompatible Time-sharing System}, le système
d'exploitation mythique développé par les hackers du MIT --- dont
faisait partie Richard \textsc{Stallman}. ITS était diffusé tout aussi
librement, mais il n'était pas portable : il ne fonctionnait guère
plus que sur les PDP-10 de la marque DEC, alors qu'UNIX fonctionnait à
terme de manière identique sur toutes les machines connectées à
ARPAnet \cite{Dib99}.

\subsubsection{Les modifications de l'université de Bekerley}

UNIX n'était au départ pas commercialisable par AT\&T, car ---
profitant d'un monopole consédé par l'état américain sur les
équipements télégraphiques et téléphoniques --- il lui était interdit
de commercialiser d'autres choses. De plus, comme la pratique en
vigueur chez les informaticiens de l'époque était de s'échanger les
programmes --- en version binaires exécutables tout autant que leurs
codes sources correspondants --- ce fut tout naturellement qu'UNIX se
retrouva sur de nombreux sites.

En 1977, Billy \textsc{Joy}, un étudiant de Berkeley\footnote{Berkeley
  est une université californienne.} effectuant depuis deux ans des
modifications importantes au système d'exploitation UNIX, rend
disponible sa version modifiée sous le nom de \emph{BSD} pour
\emph{Berkeley Software Distribution}. À travers le monde, d'autres
informaticiens installent l'UNIX de Berkeley sur leurs machines
--- plutôt que d'autres versions, d'autant que la version des Bell
Labs évoluait moins vite, leur équipe se concentrant sur la stabilité
du système d'exploitation --- effectuent à leurs tours des
modifications qu'ils renvoient à l'université de Berkeley. \cite{Dib99}

En 1978, AT\&T décide de faire payer les sources d'UNIX. Un compromis
fut trouvé avec l'université de Berkeley. Désormais, la distribution
BSD n'est seulement distribuée qu'aux institutions ayant acquis une
licence d'utilisation d'UNIX chez AT\&T. Ce qui ne posait pas alors de
problèmes car les prix étaient encore abordables.

En 1979, la DARPA recherche à unifier les systèmes d'exploitation
utilisés par les ordinateurs connectés à ARPAnet. C'est UNIX qui fut
sélectionné car il avait démontré sa portabilité. Mais c'est la
distribution UNIX BSD de Berkeley qui fut choisi.

Bekerley et Billy \textsc{Joy} deviennent donc l'épicentre du
développement de la version la plus populaire d'UNIX. En 1980, Billy
\textsc{Joy} cofonde Sun Microsystems, une entreprise dédiée à la
maintenance du système d'exploitation BSD et la commercialisation
d'une version de ce système d'exploitation :
\emph{Solaris}.\footnote{Sun devint une entreprise informatique de
  renommée internationnale, diffusant notamment le logiciel de
  bureautique OpenOffice.org ou le système d'exploitation Unix :
  Solaris. En 2011, \emph{date à vérifier}, Sun est rachetée par
  Oracle.}

\subsubsection{La guerre des UNIX}

D'autres entreprises\footnote{Dont Hewlett Packard et
  IBM. \cite{Loz10}} se lancent dans cette aventure et maintiennent
\emph{leur} UNIX ; tandis que les laboratoires Bell continuent de
maintenir et développer leur propre version.

Cette situation aboutit à ce qu'on a appelé la \emph{guerre des
  UNIX}. En effet, chaque compagnie tentant d'imposer sa version à
celles des autres, des modifications et de nouvelles fonctionnalités
furent programmées. Les versions différentes d'UNIX se distançaient
les unes des autres, et cela posa un réel problème de compatibilité
pour les programmes applicatifs qui ne fonctionnaient plus que
difficilement d'une version d'UNIX à l'autre.

C'est en 1987 que les compagnie AT\&T et Sun Microsystems conclurent
un accord en vue de fusionner leur version pour créer un nouvel UNIX
standard. En réaction à cet accord, les autres compagnies éditant une
version d'UNIX fondèrent en 1988 un consortium industriel l'\emph{Open
  Software Foundation} dans le but de créer un standard ouvert.

Ces convergences industrielles aboutirent à la publication de normes
définissant plus clairement les caractéristiques d'un système
d'exploitation de type UNIX. C'est la fameuse norme POSIX publiée la
même année par l'\emph{IEEE} (Institute of Electrical and Electronics
Engineers) qui permet aux hackers de simplifier fortement le
développement d'applications pour les systèmes UNIX.

Mais durant ce temps, IBM permet à une jeune société de conquérir le
monde en signant en 81 un contrat d'exclusivité sur le système
d'exploitation installé sur ses micro-ordinateur de type
\emph{PC}. Cette jeune société s'appelle \emph{Micro-Soft}
\footnote{L'orthographe de Micro-Soft fut modifiée ultérieurement en
  \emph{Microsoft}} et son système d'exploitation --- qui n'est pas de
type UNIX --- est nommé \emph{MS-DOS}.

\section{Les logiciels libres ont toujours existé.}

Nous découvrons ainsi que l'Histoire de l'informatique s'étend déjà
sur plusieurs siècles. Nous découvrons également que des machines
aussi complexes que les ordinateurs, ou des concepts aussi poussés que
la programmation n'ont pas été inventés en une fois par une unique
inventeur mais par la coopération de milliers d'entre eux. En effet :
\begin{itemize}
\item Sans la coopération entre informaticiens issus d'institutions
  diverses, universitaires, industrielle ou militaire, publiques comme
  privées, il n'y a pas d'ARPAnet ni d'UNIX. Et donc pas d'Internet,
  comme nous le verrons dans le chapitre suivant.
\item Sans l'implication des utilisateurs qui apportent bénévolement
  leur aide dans l'amélioration des programmes, il n'y aurait pas BSD.
\item Enfin, sans l'habitude des programmeurs à diffuser les codes
  sources à toute la communauté des informaticiens --- dans l'espoir
  que quelqu'un puisse contribuer à le rendre meilleur --- il n'y
  aurait pas d'informatique.
\end{itemize}

Il semble donc que le concept de logiciel libre est apparu avec
l'informatique, bien que ce concept n'ait été formalisé que plus tard
avec l'expression \emph{logiciel libre}.
