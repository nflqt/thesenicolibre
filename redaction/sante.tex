
\chapter{2014, la santé libre ?}
\label{chap:sante}

\parbox{0.6\textwidth}{{\sffamily {\itshape Il y a quelque chose de
      fondamental que peu de gens ont bien compris : c'est celui qui
      développe le logiciel qui décide de ce que les utilisateurs
      peuvent ou ne peuvent pas faire avec ce logiciel. Il reste à
      décider si vous voulez être un consommateur passif ou bien un
      acteur de votre vie numérique. Être un client ou devenir un
      citoyen.}}

\raggedleft Tristan \textsc{Nitot}} \vspace{4ex}

Cette conclusion est un travail essentiellement prospectif car le
logiciel libre n'a pas encore eu d'impact majeur dans la
santé. Cependant, le secteur de la santé doit se préparer à saisir les
opportunités politiques que l'informatique lui offre.

En effet, nous savons désormais que les ordinateur et le réseau
catalysent systématiquement de profondes transformations sociales dans
tous les secteurs où ils sont présents. Cela est particulièrement
évidemment concernant le commerce, l'art et la culture, ou encore
l'éducation. Puisque chaque professionnel de santé, chaque patient,
comme chaque institution de santé --- publique ou privée --- dispose
désormais d'ordinateurs et d'un accès à Internet, et qu'on en use de
plus en plus et de mieux en mieux, il faut également s'attendre à de
profondes transformations. \textbf{Tout l'enjeu pour les
  professionnels de santé est que ces transformations soient choisies
  ou subies.}

\section{Qu'est-ce que le Libre ?}

Le Libre est un courant de pensée qui prône la \emph{propriété
  commune} pour les \emph{biens immatériels}. Ce courant se décline :
\begin{itemize}
\item \textbf{en droit}, avec sa volonté de réforme des droits dits de
  \emph{propriété intellectuelle},
\item qu'\textbf{en économie}, avec sa lutte contre les monopoles et
  la promotion de modèles économiques respectueux de l'acheteur, et
  avec sa volonté de transition à une économie de services.
\item ou \textbf{en politique}, avec ses revendications concernant la
  transparence des institutions publiques et des grandes entreprises.
\end{itemize}

Le Libre doit son développement mondial aux ordinateurs et à Internet,
du fait la performance de ces machines pour la diffusion des idées ;
cela explique la place centrale de l'informatique dans ce courant. Et
c'est pourquoi il est correct de désigner le courant de pensée tout
entier par \emph{logiciel libre}.

En tout état de cause, l'avenir de la propriété des biens immatériels
--- privée ou commune ? --- constitue l'un des quelques points de
clivage politique majeurs du XXI\ieme siècle.

\section{Que peut apporter le Libre à la santé ?}

Nous allons étudier les transformations et les réponses du Libre selon
deux thèmes :
\begin{itemize}
\item \textbf{Les progiciels de santé.} Tout professionnel de santé
  utilise désormais les ordinateurs et Internet dans l'exercice de sa
  profession. Le logiciel est perçu comme un outil de travail ; mais
  dans quelle mesure peut-on espérer être maître de son outil de
  travail si aucun contrôle sur son fonctionnement n'est permis ?
  Parce qu'ils permettent le contrôle direct de leur développement par
  les utilisateurs, l'existence de progiciels libres pour chaque
  profession de santé est une condition essentielle pour la
  \textbf{défense de leur indépendance} dans le siècle à venir.
\item \textbf{les connaissances médicales.}
\end{itemize}

\subsection{Les progiciels de santé libres}

\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape L’intérêt commercial d’un
      producteur de logiciels [privateurs] est de pousser ses clients
      à acheter [...] la toute dernière version de ses produits. Il
      n’a donc aucun intérêt à ce qu’ils soient exempts de bugs ou
      même à ce qu’ils remplissent parfaitement les attentes des
      consommateurs. Son intérêt est de fabriquer un logiciel qui ne
      fonctionne que dans un environnement bien précis (parce que ça
      demandera beaucoup moins de boulot [...]) et de tendre à
      l’hégémonie. Il a ainsi tout intérêt à créer son propre langage
      [...] et de déposer un brevet sur ce langage pour éviter
      l’apparition d’une concurrence.}}

  \raggedleft Laurent \textsc{Chemla}}
\vspace{2ex}

Dans le logiciel libre, les membranes entre les différents acteurs
(développeurs, éditeurs... et utilisateurs) sont
perméables. \cite{Jea11} Les utilisateurs ont une relation directe
avec le développement de leur logiciel qui s'améliore ainsi en
fonction de leur véritable besoin. \cite{Per04}

Un logiciel libre est donc un logiciel fait par et pour ses
utilisateurs, ce qui induit un gain de \textbf{fiabilité}, et
d'\textbf{indépendance}.

\subsubsection{Le gain de fiabilité}

Une entreprise qui vend un logiciel privateur n'est pas forcément
incompétente ; un logiciel libre n'est donc pas systématiquement
meilleur que ses équivalents privateurs. Néanmoins, cela est souvent
le cas du moment que le logiciel libre en question bénéficie d'un
développement actif. Cela s'explique par la \textbf{publication du
  code source} qui induit :
\begin{itemize}
\item \textbf{un nombre potentiellement beaucoup plus élevé de
    développeurs.} Ainsi, les failles --- inévitables --- sont plus
  vite détectées puis comblées.
\item \textbf{une forte diversité des origines et des motivations des
    développeurs.} De nombreux points de vue convergents permettent un
  projet plus abouti, et annihile toute possibilité d'insertion d'une
  fonctionnalité malveillante. \footnote{Quelques exemples de
    fonctionnalités malveillantes dont il n'est pas possible d'écarter
    l'existence dans chaque logiciel privateur du fait du secret
    entourant le code source :
    \begin{itemize}
    \item la présence volontaire de code ralentissant --- ou l'absence
      volontaire d'optimisation du code --- le programme faisant
      croire à l'obsolescence du matériel ou du logiciel afin que
      l'utilisateur rachète un nouvel ordinateur ou une nouvelle
      version du logiciel.
    \item les \emph{les portes dérobées} qui permettent la fuite de
      données personnelles ---voire le contrôle de l'ordinateur ---
      via internet sans autorisation de l'utilisateur.
    \end{itemize}}
\end{itemize}

Un logiciel libre au développement actif se retrouve ainsi à terme
débarrassé d'un maximum de ses failles, rendant le piratage aussi
difficile qu'il est possible.

En fait, il s'avère que le choix entre la qualité et la liberté ne
s'impose que rarement dans l'informatique.

\subsubsection{L'impact sur l'indépendance professionnelle}

La FSF insiste régulièrement sur le fait qu'avec un logiciel libre,
c'est l'utilisateur qui est libre. \cite{Gnu} Elle veut signifier par
là, que les utilisateurs ont plus de contrôle sur leurs outils
informatiques. Dans le cas des professions de santé, les utilisateurs
sont les professionnels eux-mêmes

Nous décrivons ici 

\paragraph{Les aspects individuels}

Un utilisateur d'un logiciel privateur s'expose à plusieurs risques ou 
inconvénients :
\begin{itemize}
\item \textbf{Le viol de sa vie privée}. Il y a deux types de cas à
  étudier qui peuvent se retrouver chez un même logiciel :
  \begin{itemize}
  \item \textbf{Le cas d'un logiciel privateur qui ne publie pas ses
      sources.} Comme personne hors de la société éditrice ne sait
    comment fonctionne exactement le programme, il y a un risque que
    celui-ci contiennent des \emph{portes dérobées}, des
    fonctionnalités permettant les fuites des données, voire le
    contrôle complet de l'ordinateur via le réseau à l'insu de
    l'utilisateur. \cite{Gnu}
  \item \textbf{Le cas d'un logiciel privateur gratuit.} Dans ce cas,
    la société éditrice gagne de l'argent au moyen de la publicité
    adressée à l'utilisateur. Cette publicité peut tout à fait être
    ciblée selon les intérêts supposés de l'utilisateur qui sont
    évalués par l'étude des données personnelles recueillies durant
    l'utilisation du logiciel. \footnote{Un adage courant sur le web
      dit « Si c'est gratuit, \emph{vous} êtes le produit. »}
  \end{itemize}
\item \textbf{L'achat d'une licence d'utilisation}. Bien que ce coût
  soit masqué lors de l'achat du matériel informatique, \footnote{Ce
    qui constitue une atteinte au droit du commerce telle que la
    \emph{vente liée} ou la \emph{vente
      forcée}. \url{http://april.org/} et \url{http://aful.org/}} un
  utilisateur non-averti achète systématiquement de façon concomitante
  une licence d'utilisation pour des logiciels --- qui ont pourtant
  d'excellents équivalents libres. Plus insidieux, l'utilisateur se
  voit parfois offrir l'utilisation temporaire du logiciel, de façon à
  ce qu'il s'habitue à son utilisation, et se sente ensuite obligé de
  payer par la suite (nous rappelons encore une fois qu'il existe
  généralement des équivalents libres à ces logiciels, mais on
  maintien ainsi l'utilisateur dans l'ignorance de cette information
  cruciale.). Enfin, d'autres utilisateurs se placent volontairement
  quoi qu'inconsciemment dans l'illégalité en utilisant le logiciel
  sans licence --- c'est ce qu'on appelle le piratage --- alors que
  nous rappelons une dernière fois qu'il existe des équivalents
  libres. Ce qui signifie que le piratage n'est paradoxalement pas
  défavorable pour l'éditeur puisque l'utilisateur achètera bien un
  jour une licence du programme en question puisqu'il n'utilise pas un
  équivalent --- libre ou non --- à la place. \footnote{C'est une
    technique de dealer : offrir la première dose d'une drogue est en
    effet un investissement très rentable. Ainsi de nombreux éditeurs
    de logiciels privateurs offrent aux écoles leur programmes pour
    que ceux-ci soient enseignés aux élèves qui seront alors enchaînés
    au logiciel du fait de leur propres habitudes : il sera en effet
    très difficile pour eux d'utiliser un logiciel équivalent, libre
    ou non, puisque cela nécessite de réapprendre un nouvel outil. La
    concurrence est ainsi faussée durablement.}
\item \textbf{Une décision défavorable de l'éditeur}. L'éditeur peut
  décider d'interrompre le développement du logiciel. Cela arrive
  parfois en faveur d'une nouvelle version du logiciel aux avancées
  totalement inutiles pour l'utilisateur. Il arrive également que
  l'éditeur fasse faillite, l'utilisateur se retrouve alors avec un
  outil qui n'est plus maintenu et qui deviendra à terme inutilisable
  puisque progressivement incompatible avec le nouveau matériel ou les
  autres logiciels. \cite{Per04}
\end{itemize}

Ses risques apparaissent déjà inacceptables dans la vie personnelle,
a fortiori ils le restent durant l'exercice professionnel.

Cependant, de manière générale, ce sont les propres \emph{habitudes}
des utilisateurs qui créent leur dépendance. En effet, l'apprentissage
d'un logiciel, libre ou non, représente un investissement important en
effort, en temps et/ou en argent. On n'apprécie donc guère devoir
réitérer un tel apprentissage pour un outil équivalent. Ainsi, un
utilisateur peut être convaincu que le logiciel qu'il utilise est
moins bon qu'un équivalent et ne pas le substituer pour autant : il
est enchaîné à un logiciel ou à son prestataire informatique par la
faute de ses propres habitudes.

\paragraph{Les aspects collectifs}

\subparagraph{Le rétablissement de la concurrence sur le marché du progiciel}

Le logiciel privateur ne permet pas l'installation d'une véritable
concurrence dans le secteur de l'informatique. En effet,
l'apprentissage d'un logiciel est long et difficile. Il ne se réitère
pas régulièrement. L'utilisateur peut ainsi vouloir changer de
logiciel pour de nombreuses raisons et ne pas le faire pour
autant. Cela bloque considérablement l'arrivée sur le marché de
solutions novatrices. L'utilisation d'un logiciel privateur ne permet
guère que le choix d'un seul prestataire informatique. \textbf{C'est
  une situation de monopole économique intolérable pour les
  utilisateurs de ces programmes : les professionnels de santé et leur
  patients.}

Aucune autre solution que le logiciel libre ne pourra résoudre plus
élégamment ce problème économique puisque :
\begin{itemize}
\item \textbf{Son code source est public.} Aucune fonctionnalité
  malveillante ne peut s'y glisser sans que ce soit révélé.
\item \textbf{Ses copies ne s'achètent pas.} Les revenus des
  entreprises du logiciel libre se font par le service rendu autour du
  logiciel libre : l'installation, la maintenance et la formation. Si
  un utilisateur possède le temps et les compétences pour installer,
  maintenir et utiliser le logiciel, il n'a rien à payer.
\item \textbf{Son développement ne dépend pas d'une société
    particulière.} Si les développeurs initiaux ne s'en occupent plus,
  d'autres développeurs peuvent prendre leur place.
\end{itemize}

Ainsi dans un marché donné, seule l'existence d'un logiciel libre
rétablit un semblant de concurrence au profit des utilisateurs.

L'utilisateur d'un logiciel libre peut en effet changer de prestataire
sans changer de logiciel. La concurrence entre les prestataires se
fait ainsi sur la qualité du service proposé autour du logiciel plutôt
que sur la propriété de tel logiciel par le prestataire. \footnote{De
  plus, les prestataires en logiciel libre ont une obligation de
  conseil et de transparence vis-à-vis de leur client, renforcée par
  rapport à celle de leurs concurrents basant leur activité sur du
  logiciel privateur. \cite{Mas13}}

De plus, la facturation est beaucoup plus concrète que pour un
logiciel privateur pour lequel le lien entre le coût de la licence
d'utilisation pour l'utilisateur et les frais de l'éditeur est
complètement opaque. Avec un logiciel libre, l'utilisateur achète
directement le support : installation, maintenance et formation.

\textbf{Le rétablissement de la concurrence et la transparence de la
  facturation sont donc les deux effets économiques du logiciel
  libre.} Ils tendent vers une baisse des prix, bien que celle-ci ne
soit quantifiable qu'au cas pas cas. \footnote{Une étude menée par
  l'institut Forrester conclut néanmoins que 87\% des entreprises
  feraient des économies en utilisant des logiciels open sources.}

Finalement, avec un logiciel libre, un professionnel de santé devient
véritablement propriétaire de son outil --- numérique --- de travail,
et non plus seulement titulaire d'une licence temporaire
d'utilisation, c'est-à-dire locataire d'une certaine manière. Ce qui
garantit bien mieux son indépendance professionnelle.

\subsubsection{Le cas des données personnelles des patients}
\label{sec:donneespatients}

Les professionnels sont les utilisateurs et donc à travers eux leurs
patients.

Certains peuvent se poser la question du respect de la vie privée de
leurs patients avec l'utilisation d'un logiciel libre. Il s'agit d'une
\textbf{confusion entre le programme et les données}. Quand un
logiciel est libre, c'est la manière dont il fonctionne qui est
visible par tous, absolument pas les données qu'il utilise. Ainsi, un
logiciel libre peut tout à fait avoir pour rôle de crypter des
données.

Mieux que ça : c'est parmi les logiciels libres que figurent les
meilleurs logiciels de cryptographie. Cela s'explique là encore par la
publication du code source. \textbf{En effet, seul le chiffrement par un
  logiciel libre au code source largement connu et vérifié est digne
  de confiance. \cite{Gnu}}\footnote{Au sein du site du projet GNU,
  nous pouvons prendre connaissance de quelques pages édifiantes qui
  répertorient les défauts de sécurité inhérents aux logiciels
  privateurs. \url{http://www.gnu.org/philosophy/proprietary.html}}
Car le code source d'un logiciel privateur, connu par peu de personnes
aux intérêts inconnus, inclue à coup sûr une fonctionnalité
malveillante qui permet à un passe-partout de déchiffrer toute donnée
chiffrée par le logiciel. \footnote{Il s'agit d'une explication
  triviale du principe de \textsc{Kerckhoffs} qui explique de le
  système de chiffrement militaire doit pouvoir tomber dans les mains de
  l'ennemi, seul le secret entourant la clef de déchiffrement étant
  important. \cite{Wikipedia}}

Il nous semble clair que le patient doit avoir le contrôle exclusif de
ses données de santé ; c'est un question de déontologie
\footnote{Confère les articles 4, 45 et 46 du code de déontologie
  médicale ; ou l'article 5 du code de déontologie des pharmaciens.}
C'est par exemple le patient qui doit décider à quel professionnel il
fait confiance. Il faut donc un modèle informatique de sécurité des
données personnelles numérisées centré sur le patient, qui serait le
seul à posséder la clef qui chiffre ses propres données, et qui décide
nominativement qui peut y avoir accès. Parce que nous avons
l'accès et le contrôle sur le code source, seul le logiciel peut nous
garantir ce fonctionnement..

\subsection{L'information médicale libre}

\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Dans la mesure où le but
      d'une information scientifique est d'être diffusée, la
      publication [par les revue] doit être considérée à sa juste
      place : un acte accessoire motivé par d'autres raisons que
      l'avancement des sciences.}}

  \raggedleft Christophe \textsc{Masutti}} \vspace{2ex}

Nous devons faire une distinction très claire entre :
\begin{itemize}
\item \textbf{Les données médicales personnelles des patients}, qu'il
  ne s'agit bien évidemment pas de rendre visible aux yeux de tous.
  Le cas de ces données a été traité précédemment au paragraphe
  \ref{sec:donneespatients} page \pageref{sec:donneespatients}.
\item Et \textbf{les données scientifiques médicales} qui constituent
  le sujet de cette sous-section.

\end{itemize}

Depuis peu, la publication papier n'est plus le support le plus
efficace pour la diffusion de l'information scientifique ni pour la
communication entre les scientifiques autour du monde. En effet, avec
un équipement abordable par tous, un ordinateur personnel et une
connection à Internet :
\begin{itemize}
\item la mise en forme des données scientifiques produites est
  réalisée par les auteurs eux-mêmes,
\item la diffusion de ces données peut se faire instantanément.
\end{itemize}

Puisqu'il n'y a plus de freins techniques à la diffusion sans
limite de l'information scientifique, les productions scientifiques
devraient donc circuler librement au sein de la communauté
scientifique comme en dehors.

Pourtant, l'information scientifique continue d'être largement
inaccessible au public : certains établissements publics
\footnote{Tels que la \emph{DEQM}, la \emph{Direction Européenne de la
    Qualité du Médicament et des Soins de santé}, l'agence du conseil
  de l'Europe qui édite la pharmacopée européenne, et dont les bases
  de données ne sont pas librement accessibles, même aux citoyens des
  pays membres. \url{https://www.edqm.eu/}} comme les sociétés
éditrices de revues scientifiques continuent à profiter d'une rente de
moins en moins justifiée puisque :
\begin{itemize}
\item le travail effectué est faible et peu coûteux : le format
  numérique ne coûte rien, et ni les auteurs ni les membres des
  comités de sélection ne sont rémunérés par les revues.
\item les prix des abonnements augmentent néanmoins. \cite{Mas10}
\end{itemize}

En fait, puisque la recherche est largement financée par le
contribuable --- si ce n'est entièrement dans le cas des médicaments
puisque les revenus des laboratoires privés se font essentiellement
via l'assurance maladie \cite{Smi09} --- quelle est la légitimité à ce
que ses résultats ne soient pas accessibles à tous ? Et cela est
d'autant plus vrai concernant les connaissances médicales qui sont
nécessaires pour que chaque patient puisse effectuer des choix de
santé de façon éclairée.

Des associations promeuvent l'accès des connaissances
médico-pharmaceutiques sans restriction, d'autres diffusent déjà
librement les fruits de leur travaux :
\begin{itemize}

\item L'association \emph{Tela Botanica} qui propose en licence libre
  tout un corpus de connaissances botaniques sur son site
  web. \footnote{La licence libre utilisé pour le contenu est la
    CC-BY-SA tandis que \emph{Papyrus} le logiciel libre utilisé pour
    la forme du site web est publié selon les termes de la
    GPL. \url{http://tela-botanica.org/}}
\item L'encyclopédie \emph{Wikipedia} consacre un portail à la
  médecine.
\end{itemize}

\section{Comment le secteur de la santé peut-il migrer au libre ?}

\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Le choix d’un système
      ouvert et libre peut [...] rendre nos entreprises plus
      compétitives [...]  Cela peut créer [...] un espace de
      croissance et des emplois qualifiés pour des ingénieurs qui
      seront responsables de la qualité de leurs produits... et pas
      seulement pour des commerciaux qui essaient de vendre un produit
      sur lequel ils n’ont aucun contrôle, et dont les bénéfices
      partent vers Seattle.}}

  \raggedleft Roberto \textsc{Di Cosmo}} \vspace{2ex}

La \emph{migration} est le terme consacré en France pour désigner
l'action d'installer et d'utiliser un équivalent libre à la place d'un
outil privateur qu'on utilisait avant de prendre conscience des
dangers que cela représente.

Cette migration au Libre des professionnels de santé sera une démarche
à la fois\textbf{individuelle et collective}.

\subsection{Individuellement}

C'est la prise de conscience suivie d'action concrète de chacun des
professionnels de santé qui sera le moteur de la migration au
Libre. Chaque professionnel peut pour lui-même :
\begin{itemize}
\item \textbf{Refuser les logiciels privateurs} lorsqu'un équivalent
  libre est disponible,
\item \textbf{Participer au développement de solutions libres} ; en
  sachant que le développement de logiciels fait appel à de nombreuses
  connaissances non informatiques, rédaction, traduction, graphisme,
  son, mais aussi gestion, finance et droit, ou encore des
  connaissances médicales dans le cas d'un logiciel de
  santé. \footnote{Par exemple, Éric Maeker, médecin généraliste
    exerçant en Belgique, est également l'administrateur et le
    développeur principal de la suite logicielle médicale open source
    \emph{FreeMedForms}.}
\item \textbf{Migrer le plus tôt possible} ; Car ce n'est pas après
  avoir fournis de nombreux effort pour maîtriser un premier progiciel
  malheureusement privateur qu'on a du temps à consacrer à un tel
  changement. Dans l'idéal, la migration doit débuter dès les études
  puisque chaque besoin d'un étudiant --- même débutant dans
  l'utilisation des ordinateurs --- est aujourd'hui entièrement comblé
  par un logiciel libre au moins. Et il faut ajouter qu'il n'a aucun
  sens à utiliser un progiciel libre au sein d'un système
  d'exploitation privateur.\footnote{Nous conseillons une migration
    progressive sur le long terme qui passe aujourd'hui par des stades
    dont la durée peut varier de quelques mois à une ou deux années :
    \begin{enumerate}
    \item L'installation au sein du système d'exploitation habituel de
      son ordinateur personnel d'un maximum de logiciels libres --- ou
      quasiment libres --- dont il existe des versions au sein des
      systèmes d'exploitation libres : Firefox, Thunderbird,
      LibreOffice, Klavaro, VLC media player, Gimp, Scribus et
      Inkscape par exemple. (durée conseillée : six mois)
    \item L'installation d'une distribution GNU/Linux adaptée aux
      débutants sur une partition distincte du disque dur de son
      ordinateur. Cette procédure, désormais courante, est appelé un
      \emph{dual-boot} : au démarrage de l'ordinateur, le choix entre
      les deux systèmes d'exploitation sera proposé. (durée conseillée
      : six mois)
    \item La suppression du système d'exploitation privateur pour ne
      laisser qu'un système d'exploitation GNU/Linux sur
      l'ordinateur. (durée conseillée : au moins un an)
    \item L'installation d'une distribution GNU/Linux totalement libre
      à partir de la liste validée par la FSF qu'on peut trouver à
      cette adresse :
      \url{http://www.gnu.org/distros/free-distros.html}
    \end{enumerate}}
\end{itemize}

\textbf{Cependant en 2013, les solutions libres de progiciels de
  santé, lorsqu'elles existent,souffrent d'un manque cruel de
  visibilité}, ce qui explique leur faible utilisation actuelle par
les professionnels de santé. D'autre part, une \emph{migration}
décisive des professions de santé aux logiciels libres ne peut passer
que par une volonté éclairée, constante et déterminée d'un minimum de
personnes concernées. Par exemple, la migration isolée d'un
professionnel ne permettra jamais l'ouverture des spécifications de la
carte vitale. \footnote{Aujourd'hui, la carte vitale ne peut pas être
  directement prise en charge par un logiciel libre, les médecins
  généralistes peuvent par contre utiliser des boîtiers autonomes.}
Les institutions du secteur de la santé ont donc certainement un rôle
important à jouer.

\subsection{Collectivement}

Puisque les logiciels libres en santé subissent un déficit de
visibilité, les institutions doivent prendre certaines
responsabilités. Le problème peut être résolu de manière collective
par un fort soutien :
\begin{itemize}
\item \textbf{des institutions privées} telles que syndicats,
  groupements de praticiens ou diverses associations,
\item \textbf{des institutions publiques} telles que les universités,
  facultés, écoles, ou les conseils ordinaux.
\end{itemize}

Il est malheureusement d'usage pour les institutions publiques
d'invoquer l'excuse de la \emph{neutralité technologique} pour ne pas
soutenir sans réserve les logiciels libres. Cette vague notion n'est
pourtant rien à côté de la \emph{neutralité commerciale} dont doivent
faire preuve les pouvoirs publics.

Car c'est justement le soutien au logiciel libre qui permet aux
institutions de garder leur neutralité. En effet,
\begin{itemize}
\item \textbf{toute entreprise peut fournir des services basés sur des
    logiciels libres}... puisqu'ils sont libres.
\item De plus, si une entreprise ne désire pas adapter à d'autres
  logiciels les services qu'elle propose aux institutions,
  \textbf{elle peut très bien libérer son propre logiciel}.

\end{itemize}

Parce que le Libre constitue le seul système qui n'entraîne pas un
monopole : changer de logiciel, on peut changer de prestataire.

Et parce que l'argent du contribuable dans le cas du public, et
l'argent collectif des professionnels de santé dans le cas du privé
devrait allé dans un logiciel dont on est sûr qu'il respecte les
libertés de ses propres utilisateurs.

\subsubsection{Le cas des établissements d'enseignement supérieur}

\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape L'école a la mission
      sociale d'éduquer de bons citoyens d'une société forte, capable,
      indépendante, solidaire \emph{et libre}.}}

\raggedleft Richard \textsc{Stallman}}
\vspace{2ex}

Les établissements d'enseignement supérieur de santé ont la
responsabilité de refuser le logiciel privateur et de n'utiliser que
les logiciels libres :
\begin{itemize}
\item \textbf{Pour les ordinateurs à disposition de leurs
    étudiants}. Ceci est atteignable de nos jours où toutes les tâches
  nécessaires à la réalisation d'études de santé peuvent être
  réalisées via les logiciels libres. Mieux : un choix est souvent
  possible entre plusieurs logiciels libres pour une même tâche. Pour
  n'en citer que deux, nous avons les suites bureautiques
  \emph{LibreOffice} et \emph{Open Office}
\item \textbf{Pour leurs cours d'informatique.} D'une part, ce n'est
  pas rendre service aux étudiants que de leur apprendre à se servir
  d'outils qu'ils ne contrôleront pas dans leur vie active, mais qui
  peuvent servir à les contrôler, eux. \cite{Sta13} \cite{Gnu} D'autre
  part, cela place les enseignants dans un \emph{conflit d'intérêt} en
  faveur des entreprises de logiciels privateurs.
  \begin{itemize}
  \item soit il y a beaucoup trop de logiciels sur tel marché pour
    trop peu d'heure d'enseignement, et les enseignants doivent
    sélectionner les logiciels qu'ils enseigneront. Au mieux, cela
    transforme les enseignants en \emph{commerciaux à titre gracieux}
    pour les éditeurs de logiciels privateurs. Bien pire : cela incite
    à la \emph{corruption} des enseignants par les éditeurs. Il faut
    trouver une solution à cette équation apparemment insoluble entre
    le manque d'heures d'enseignement suffisante pour faire le tour de
    tous les concurrents d'un marché donné, et l'obligation de ne pas
    favoriser tel concurrent.
  \item Soit il y a trop peu de logiciels sur un marché dominé par un
    ou quelques éditeurs. Et l'enseignant participe alors au
    renforcement du monopole en n'enseignant que le ou les logiciels
    les plus connus. Or en informatique plus encore que dans d'autres
    secteurs, ce sont des entreprises peu visibles qui sont cependant
    les plus innovantes. L'établissement freine ainsi le progrès, ce
    qui va à l'encontre de sa mission.
  \end{itemize}
  Puisque toute entreprise peut baser ses services sur un logiciel
  libre, l'établissement ne fausse pas la concurrence en enseignant
  uniquement avec des logiciels libres. Le logiciel libre est la
  solution à l'équation ci-dessus.
\end{itemize}

Enfin, s'il n'a pas de logiciel libre disponible pour une tâche
précise --- c'est un cas désormais rare mais c'est pourtant encore
vrai en pharmacie d'officine --- un établissement d'enseignement
supérieur doit :
\begin{itemize}
\item tourner ses cours de façon à ce qu'ils ne nécessitent pas
  l'usage d'un logiciel particulier.
\item apporter des moyens humains, matériels ou financiers à
  l'élaboration d'un logiciel libre de façon à ce que les futurs
  professionnels de santé qu'il forme puissent un jour exercer en
  toute indépendance la part informatique de leur métier.
\end{itemize}

\textbf{De façon générale, enchaîner le futur exercice professionnel
  de ses étudiants à tel éditeur informatique est une faute pour
  l'enseignement supérieur de santé.}\footnote{Il convient de nuancer
  ce propos dans le cas d'une circulaire ministérielle qui incite les
  universitaires à utiliser le logiciel privateur, ou qui met
  simplement les solutions privatrices au même niveau que les
  solutions libres. En effet, dans ce cas, la faute professionnelle
  incombe plutôt aux politiciens rédacteurs d'une telle
  circulaire. Car l'intérêt général en matière d'informatique passe
  par le logiciel libre. Il serait néanmoins hors du cadre de cette
  thèse de démontrer cela, quoi que quelques pistes figurent au
  chapitre \ref{chap:libre} \pageref{chap:libre}.}

Néanmoins, il ne semble pas contraire à l'éthique pour un tel
établissement
\begin{itemize}
\item \textbf{De se servir de logiciels privateurs pour ses besoins de
    fonctionnement} --- nous entendons par là toute tâche qui ne
  relève pas de l'enseignement --- s'il n'existe pas encore de
  logiciel libre équivalent. Mais bien entendu, l'établissement doit
  garder la volonté --- pour garantir sa propre indépendance
  professionnelle --- d'atteindre un jour l'objectif du cent pour cent
  libre. \footnote{Après tout, les développeurs du projet GNU n'ont
    pas fait autrement lorsqu'ils ont développé composant après
    composant un remplaçant libre des UNIX privateurs. C'est la
    volonté d'aller vers le 100\% libre qui est importante. Ainsi,dans
    les cas où cela s'avère nécessaire et temporaire, nous pouvons
    utiliser des logiciels privateurs. Mais n'oublions pas que ces cas
    sont désormais rares en 2014.}  En tout état de cause, le
  remplacement d'un logiciel libre par un logiciel pour une tâche
  donnée, constitue une faute professionnelle pour le service
  informatique d'un établissement d'enseignement supérieur.
\item Ou encore, \textbf{de louer ses locaux pour des séminaires
    privés} si ceux sont réalisés par les propres formateurs des
  éditeurs de logiciels privateurs.
\end{itemize}

Nous voyons ainsi que la question du logiciel libre par les
établissements d'enseignement va au-delà de la sélection d'une marque
ou d'une autre ; il ne s'agit rien de moins que de faire les choix
compatibles avec l'obligation de neutralité commerciale du service
publique et les principes d'universalité sur lesquels se bâtit
l'enseignement supérieur en France. \cite{Gnu} \cite{Wikipedia}

\subsubsection{Le cas des associations étudiantes}

Les associations d'étudiants de type BDE et corpos, que l'on rencontre
dans presque chaque établissement d'enseignement supérieur de santé
ont toutes pour objet de \textbf{défendre les intérêt moraux et
  financiers} de leurs adhérents. Puisque le logiciel libre défend les
intérêts des utilisateurs des ordinateurs, l'utilisation et la
promotion des logiciels libres rejoint les intérêts des adhérents des
associations étudiantes en particulier. Néanmoins, ces associations
ont également pour valeur de ne pas s'impliquer dans les clivages
politiques, or la promotion du logiciel libre est un engagement
éminemment politique qui ne concerne pas seulement les étudiants mais
bien la société entière.

Cependant, la plupart de ces associations ont un budget serré qui ne
leur permet pas l'achat de licences d'utilisation de logiciels
privateurs. \textbf{L'utilisation exclusive de logiciel libre leur
  permet alors quelques économies dont elles doivent profiter sans
  pour autant s'engager dans la promotion des logiciel libres.}

D'autant que toutes les tâches d'une associations étudiantes peuvent
aujourd'hui être réalisées avec les logiciels libres. Citons-en
quelques-uns :
\begin{itemize}
\item \textbf{Dolibarr} pour la trésorerie et la gestion des
  adhésions,
\item \textbf{Inkscape}, pour la réalisation de logos,
\item \textbf{Gimp}, pour la réalisation d'affiches,
\item \textbf{Scribus}, pour la maquette d'un journal associatif,
\item \textbf{Blender}, pour le montage vidéo,
\item \textbf{Wordpress} ou \textbf{PluXml} pour la réalisation de
  sites web.
\end{itemize}

\subsubsection{Le cas des groupements professionnels}

Les professionnels de santé --- particulièrement les pharmaciens
d'officine --- se regroupent souvent dans une structure collective
dont le but est de 
\begin{itemize}
\item commander ensemble les médicaments pour mieux négocier les prix,
\item créer des enseignes.
\end{itemize}

Avec le logiciel libre, les groupements peuvent aussi étendre leurs
offres en proposant la maintenance des ressources informatiques et la
formation de leurs membres.

\textbf{En matière d'informatique, être le client ne suffit pas pour
  que le prestataire respecte l'utilisateur}. En effet, il faut que
les professionnels de santé maîtrisent également :
\begin{itemize}
\item \textbf{le développement du logiciel} directement en participant
  --- moralement, financièrement, ou humainement --- à la communauté en
  charge de ce développement,
\item \textbf{l'offre de services} basés sur les logiciels en étant
  actionnaires du prestataire.
\end{itemize}

Et cela de façon collective, puisqu'un tel contrôle individuel semble
illusoire. \textbf{Avec le logiciel libre, les groupements de
  professionnels de santé disposent d'un atout majeur qu'il serait
  préjudiciable de ne pas jouer.}

\subsubsection{Le cas des syndicats}

Les syndicats sont attachés à leur neutralité politique et ils
pourraient hésiter à s'engager dans le débat de la propriété
intellectuelle. Ils n'ont en fait même pas à le faire : les logiciels
libres ont été libérés par leurs propres auteurs.

Les syndicats sont également attachés à leur neutralité
commerciale. Et ils pourraient refuser de citer des entreprises basant
leurs services sur les logiciels libres. Là non plus, ils n'ont pas à
le faire : ils peuvent citer les noms des logiciels en question
puisque ceux-ci sont libres, toute entreprise informatique peut en
vivre.

En fait, les syndicats des professionnels de santé ne doivent pas
hésiter à s'engager dans la \textbf{promotion des logiciels libres
  chez leurs adhérents}, d'autant qu'il s'agit de faire comprendre
l'opportunité de gain d'indépendance que ces logiciels offrent.

