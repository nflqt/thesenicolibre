
\chapter[Conclusion : l'avenir de la santé]{Conclusion :\\l'avenir de
  la santé}
\label{chap:conc}

\parbox{0.6\textwidth}{{\sffamily {\itshape Ce serait l'une des plus
      belles opportunités manquées de notre époque si le logiciel
      libre ne libérait rien d'autre que du code.\footnotemark}}

  \raggedleft Hannu \textsc{Putonnen}} \vspace{4ex}

\footnotetext{Cette citation est issue du documentaire \emph{Nom de
    code : Linux} réalisé par Hannu \textsc{Putonnen} et paru en
  2002. Durant quelques années, elle était devenue le slogan du blog
  de l'association framasoft. (\url{http://www.framablog.org/})}

Combien de professionnels de santé utilisaient un ordinateur dans le
cadre de son exercice il y a 30 ans ? A peu près aucun et aujourd'hui,
à peu près tous. Internet, il y a quinze ans ? À peu près aucun et
aujourd'hui, à peu près tous. Nous nous donnons rendez-vous dans
quinze ans pour constater à quel point l'informatique s'est insinuée
dans l'exercice des professionnels de santé.

Si l'informatique prend ainsi une importance croissante dans nos
métiers et dans nos vies, il est particulièrement ardu de distinguer
le véritable enjeu politique et syndical derrière l'écran de fumée
pseudo-technique qui étouffe les simples utilisateurs des ordinateurs
que sont les professionnels de santé.

Car nous vivons une révolution authentique, et de surcroît
fulgurante. Les questions politiques, économiques et sociales autour
des ordinateurs et d'internet figurent déjà parmi les plus
importantes, au même niveau que les questions foncières, fiscales ou
économiques.

\textbf{Est-ce que nous, professionnels de santé, préfèrons être
  locataires de nos locaux et nos outils de travail ou en être
  propriétaire ?} Après tout, nous pourrions aussi bien user de locaux
et d'outils loués, n'est-ce pas ? Pourtant ne pas en être propriétaire
nous rebute presque systématiquement : il s'agit en fait d'une
\textbf{question d'indépendance}.

\textbf{Nous pouvons raisonner ainsi pour le numérique, bien que par
  ignorance, nous ne nous en soyons pas encore rendu compte.} Or c'est
dès aujourd'hui que nous devons prendre les mesures préserveront notre
indépendance professionnelle : Nous devons nous forger des logiciels
libres et les utiliser.

Et il y a encore plus important encore que l'indépendance
professionnelle : la vie privée des patients. Nous avons en effet la
charge de données personnelles sensibles ; nos outils numériques se
doivent d'être fiables concernant la sécurité de ces
données. \textbf{Seuls les logiciels libres, parce que leur
  développement est transparent, peuvent nous le garantir.}

\section*{Le logiciel libre de santé }

De nombreuses problématiques informatiques percent dans le secteur de
la santé, nous sommes encore une fois convaincu qu'elles peuvent
trouver leur solution dans le monde du Libre. À partir du travail
réalisé pour cette thèse, nous pouvons explorer les sujets suivants :
\begin{itemize}
\item \textbf{Les logiciels des appareils implantables.} Peut-on
  permettre l'opacité des codes sources pour les logiciels utilisés
  sur un ordinateur qu'on implante dans le corps d'un être humain ? En
  effet, dans ce cas, il semble juste de demander aux logiciels le
  même niveau de transparence qu'on demande aux médicaments.

  Car le secret n'a pas cours dans le domaine médical, nous avons tous
  accès à la connaissance sur ce qu'on avale pour se soigner. En
  effet, un brevet pharmaceutique, octroie certes à un laboratoire une
  exclusivité temporaire de vingt ans sur l'utilisation de la molécule
  qu'il a découverte ou inventée. Mais ce droit ne s'acquière qu'en
  échange de la transparence totale : la publication. C'est cela le
  brevet. Ce qui permet aux laboratoires concurrents --- comme à
  l'ensemble de la société, et en particulier comme à tout
  professionnelf de santé --- de connaître exactement le
  médicament. Les concurrents doivent seulement attendre quelques
  années avant d'être autorisé à en faire commerce.

  Or un brevet qui coure 20 ans ne serait pas adapté au monde du
  logiciel. En effet, très peu d'entre eux gardent un intérêt
  commercial au bout d'une période aussi longue. Un brevet logiciel de
  20 ans équivaut finalement à un brevet perpétuel, ce qui est
  contraire à l'intérêt général puisque cela constitue des monopoles
  économiques. Et c'est exactement ce qu'on peut constater avec le
  quasi-monopole de Microsoft. En fait, dans le monde du logiciel, un
  brevet beaucoup plus court serait plus adapté ; tellement court
  qu'il serait inexistant : \textbf{Le logiciel libre constitue le
    seul compromis juste --- ne serait-ce qu'en matière de santé, dans
    le secteur de l'informatique.} En tout état de cause, ce sujet des
  \textbf{logicaments} mérite qu'on lui consacre un mémoire de fin
  d'étude.
\item \textbf{La sécurité des donnés de santé des patients.} Il nous
  semble clair que la seule personne qui doit avoir le contrôle sur
  ces données est le patient lui-même. C'est la déontologie qui nous
  le commande. C'est en effet le patient qui doit décider à quel
  professionnel il fait confiance ou pas. Il faut donc imaginer un
  modèle de sécurité des données centré sur l'individu-patient, qui
  serait le seul à posséder la clef de chiffrement de ses données à
  lui, et qui décide qui peut avoir accès ou pas. Bien sûr, le code de
  ce modèle doit être libre, pour que les patients aient le contrôle
  sur la façon dont leurs données sont chiffrées.
\item \textbf{L'auto-hébergement des données des patients.} Les
  professionnels de santé peuvent-ils laisser à des prestataires ---
  même aggréés --- les données personnelles --- même chiffrées --- de
  leurs patients ?  Voici encore un sujet à explorer.
\end{itemize}

Chacune de ces pistes pourrait encore constituer un sujet de mémoire
ou de thèse d'exercice pour un étudiant en santé du XXI\ieme
siècle. \textbf{Une certitude : le code source doit être libre.}

\section*{Le Libre comme inspiration aux réformes de santé}

Enfin, si le Libre n'a encore que peu d'impacts sur le secteur de la
santé, nous sommes convaincus que son influence s'intensifiera dans
les décennies à venir, jusqu'à devenir le moteur de la transformation
des métiers de santé. Le Libre rassemble en effet des solutions
pertinentes que nous pourrions adapter aux problèmes incessants et
croissants que subissent nos professions :

\begin{itemize}
\item \textbf{Le rognage des droits réservés.} Nous pensons tout
  particulièrement à celui de vendre des médicaments qui est
  actuellement réservé aux entreprises appartenant à des titulaires du
  diplôme de pharmacie. En effet, grâce à l'étude de l'économie des
  logiciels libres --- qui est un modèle en lutte contre les monopoles
  des sociétés éditrices de logiciels privateurs --- nous pouvons
  comprendre que ce qu'on appelle à tort monopole pharmaceutique n'est
  absolument pas un monopole économique. \textbf{Et qu'au contraire,
    l'extension de ce droit réservé de vente des médicament ne peut
    qu'aboutir à un marché du médicament tenu par un cartel de
    quelques grands groupes détenteurs des chaînes de pharmacie, à
  l'image de celui de l'informatique non-libre.}
\item \textbf{Les restrictions du libre choix du patient} tels que les
  contrats qui obligent peu à peu les patients à ne se faire soigner
  qu'avec les dentistes conventionnés de leur mutuelle. Ces contrats à
  la limite de la légalité font penser à ceux qui lient les
  utilisateurs des logiciels privateurs : \textbf{voici comment la
    liberté est amputée par l'avidité de certaines entreprises
    profitant de l'ignorance des consommateurs, et voire même de celle
    des syndicats sensés les protéger.}
\item \textbf{Le déclin de la participation des caisses de Sécurité
    Sociale.} En appliquant le Libre à la recherche pharmaceutique,
  nous pourrions mieux répartir les frais de recherches entre les
  différents acteurs du développement d'une seule molécule (et même
  les diminuer par la suppression des doublons). Ce qui sera autant de
  dépenses qui ne pèseront plus sur le coût du médicament. \textbf{Le
    \emph{ médicament libre} permettrait d'importantes économies.}
\item \textbf{L'absurdité de d'«information des professionnels»
    assurée par les laboratoires} qui sont ainsi juge et partie. Avec
  une licence libre pour le médicament, la compétition entre les
  laboratoires ne se fera plus sur la possession de tel ou tel brevet,
  mais sur leur réelle capacité à produire du médicament. Et ces labos
  cesseront d'investir une part énorme de leur budget dans une
  information parasitée par la publicité. Et ce champ de l'information
  médicale, laissé vacant par les laboratoires, deviendrait alors un
  nouvel eldorado pour des entreprises dédiées --- de taille plus
  modeste et surtout plus neutres concernant le
  médicament. \textbf{Les établissement d'enseignement supérieur
    saisiront eux aussi une belle occasion de renforcer leur
    participation à la formation continue de leurs anciens étudiants.}
\item \textbf{La publicité concernant les médicaments et les produits
    de santé} est-elle une source d'information fiable ? Qu'elle
  s'adresse aux patients ou aux professionnels de santé, n'interfère
  t-elle pas avec la véritable information médicale scientifique ? Le
  nom de fantaisie des médicament est-il toujours un mode de publicité
  tolérable ? \textbf{La substitution ne serait-elle pas plus aisée si
    les princeps ne se vendaient désormais plus que sous la
    dénomination commune internationale ?}
\end{itemize}

L'intense fréquentation des logiciels libres créent un immense espoir
\textbf{tout simplement parce qu'ils fonctionnent !} Et probablement
mieux que leurs équivalents privateurs. Le Libre est en fait une
utopie vivante, qui se construit chaque jour par l'arrivée de
centaines de nouveaux participants à chacun des projets libres,
informatique ou non. Un ancien étudiant en pharmacie se prend alors à
réver à la libération que son secteur professionnel pourrait connaître
un jour...
