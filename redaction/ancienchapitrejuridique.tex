
\chapter{Le Libre et le droit}
\label{chap:droit}

\marginpar{Sources intéressantes pour cette partie :
\begin{itemize}
\item La partie sur les licences du bouquin historique de Framasoft,
\item Le bouquin Option Libre de Benjamin Jean,
\item Le site gpl-violation.org,
\end{itemize}}

Or dans une société où l'information et la connaissance prennent une
importance capitale, ce clivage s'est étendu hors du secteur du
logiciel. \marginpar{Étudier les notions de société de l'information
  et de société de la connaissance.}

Ainsi, une querelle des anciens et des modernes a lieu aujourd'hui
concernant le droit des idées et œuvres immatérielles. Nous avons :
\begin{itemize}
\item d'un côté, les défenseurs d'une extension des droits dits de
  \emph{propriété intellectuelle} existants \footnote{Eben
    \textsc{Moglen}, professeur de droit à l'université Columbia,
    président du Software Freedom Law Center, et amateur de
    science-fiction les appelle les \emph{IPdroïds} : les androïdes de
    la propriété intellectuelle. Car, à l'instar des robots de
    protocole du film /emph{Star Wars}, ils agissent de la façon dont
    ils ont été programmés même si c'est en dépit du bon sens.}
\item de l'autre, les \emph{libristes} qui veulent assouplir, diminuer
  voire supprimer ces droits.
\end{itemize}

Nous étudierons donc dans ce chapitre les notions de droits sur
lesquelles se base cette querelle juridique. Nous apprendrons d'abord
quelques notions juridiques \emph{(section \ref{sec:droit})} qui nous
aideront à mieux comprendre le droit en général. Ces notions seront
nécessaires pour aborder par la suite les droits dits de propriété
intellectuelle et les fondements juridiques du Libre : les licences
libres. \emph{(section \ref{sec:pil})}.

\section{Le droit, la liberté et la propriété}
\label{sec:droit}

\vspace{4ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Chaque fois qu'il y a
    conflit entre les droits humains et les droits de la propriété,
    les droits humains doivent prévaloir.}}

\raggedleft Abraham \textsc{Lincoln}}
\vspace{4ex}

Dans une société humaine, les interactions entre les individus
obéissent à des idées érigées en règles définissant ce qui est
autorisé et ce qui est interdit. Pour certaines de ces règles, un
accord tacite entre les individus est suffisant pour assurer leur
respect global ; il s'agit de la \emph{politesse} ou de la
\emph{morale}. Pour d'autres --- peut-être plus importantes ou plus
complexes --- une formulation précise est nécessaire pour éviter les
litiges. L'ensemble de ces règles formulées sans équivoque est le
\emph{droit} ; ces règles sont donc dites \emph{règles de droit} ;
enfin, l'adjectif qui fait référence au droit est \emph{juridique}.

\subsection{Le droit}

Le droit semble être une \emph{entité} difficile à décrire, et nous
pensons qu'un travail de classification préalable peut nous aider.

\subsubsection{La classification du droit selon son origine
  métaphysique}

Cette classification est plus philosophique que pratique ; elle est le
reflet des oppositions entre la conception universaliste et la
conception relativiste du monde.

Elle distingue :
\begin{itemize}
\item le \emph{droit naturel} qui échoit aux hommes par la Nature ou
  par la volonté d'un Dieu et qui a une vocation universelle : il doit
  s'appliquer en tout temps et en tout lieu.
\item du \emph{droit positif} qui a été conçu par l'Homme lui-même.
\end{itemize}

Les \emph{déclarations des droits humains} ainsi que les \emph{droits
  de l'environnement} tiennent plutôt d'un droit naturel. Alors que
les \emph{normes techniques} tiennent d'un droit positif. Concernant
une œuvre de l'esprit, le \emph{droit moral}, branche du \emph{droit
  d'auteur}, tient d'un droit naturel. Tandis que les droits
patrimoniaux, autre branche du droit d'auteur, tiennent du droit
positif.

\subsubsection{La classification du droit selon son objet}

Il faut également distinguer :
\begin{itemize}
\item le \emph{droit objectif}, qui est l'ensemble des règles de
  principe abstraites et obligatoires qui régissent la vie des hommes
  en société,
\item et les \emph{droits subjectifs} sont les prérogatives concrètes
  dont une personne juridique peut se prévaloir.
\end{itemize}

Les droits subjectifs d'une personne juridique se divisent :
\begin{itemize}
\item en \emph{droits extrapatrimoniaux},
\item et en \emph{droits patrimoniaux}.

\end{itemize}

Les droits extra-patrimoniaux sont intransmissibles, incessibles et
imprescriptibles. Ce sont par exemple le \emph{droit à la vie privée},
et le \emph{droit moral} d'un auteur sur son œuvre.

Tandis que les droits patrimoniaux sont évaluables en argent et
échangeables : Parmi ces droits patrimoniaux, on compte les propriétés
littéraire et artistique, et la propriété industrielle. Les droits
patrimoniaux sont eux-mêmes divisés :
\begin{itemize}
\item en \emph{droits réels} qui s'appliquent sur une chose, un bien,
\item et en \emph{droits personnels} qui s'appliquent sur une autre
  personne.
\end{itemize}

\subsubsection{La classification du droit selon son application} 

Cette classification du droit dépend de l'histoire juridique des
pays. En effet, chaque pays présente un droit propre et ses traditions
juridiques. Ces différents droits ont une histoire de plusieurs
centaines voire milliers d'années. Ainsi, notre droit français descend
notamment du droit de l'empire romain, et il partage donc certaines
caractéristiques avec d'autres droits contemporains également issus du
droit romain.

Aujourd'hui avec la mondialisation, ces différents droits cohabitent,
s'affrontent, ou se mélangent à un rythme soutenu. Si bien que la
nécessité d'une harmonisation mondiale des droits nationaux se fait
sentir. En 1949, émerge ainsi une déclaration universelle des droits
humains. Parallèlement, les pays d'europe établissent progressivement
un droit européen, de part le conseil de l'europe et l'union
européenne.

Bien qu'il existe de nombreux ensembles historiques de droits, les
deux qui concernent le plus l'objet de notre thèse sont :
\begin{itemize}
\item Les \textbf{romano-germaniques} ou \textbf{latins}, pour
  lesquels un parlement est la source principale d'émission des
  règles, c'est un droit appelé \emph{civiliste} ou \emph{législatif}
  pour cela.
\item Les \textbf{systèmes anglo-saxons}, pour lesquels les règles
  prééminentes sont constituées par la jurispridence : l'ensemble des
  décisions précédentes établies par les juges ; ce type de droit est
  ainsi appelé \emph{jurisprudentiel}. 
\item Dans de nombreux pays, le droit est influencé par les deux types
  précédents. Celui-ci est alors dit \emph{mixte}.
\end{itemize} \marginpar{Ajouter les différences dans la façon de
  juger : entre procédure accusatoire et procédure inquisitoire.}

\textbf{Au sein du droit français}, droit de type romano-germanique,
on différencie ainsi le \emph{droit public} qui traite spécifiquement
de l'intérêt général, du \emph{droit privé} qui traite des intérêts
particuliers.

\begin{itemize}
\item Le droit public rassemble le \emph{droit constitutionnel} qui
  décrit l'État, et le \emph{droit administratif} qui décrit les
  diverses autres institutions publiques.
\item Le droit privé rassemble par exemple le \emph{droit civil} qui
  régit les interactions entre des individus, le \emph{droit des
    affaires} pour les des entreprises commerciales, le \emph{droit du
    travail} pour les rapports entre employeurs et employés.
\end{itemize}

La limite de cette distinction est constituée par les nombreux
chevauchements entre ces deux droits. De plus, il existe des branches
complètes du droit qui peuvent être classées aussi bien dans le droit
privé que dans le droit public ; ainsi, le \emph{droit pénal} décrit
les infractions éventuellement commises par les personnes et la
sanction de la société en réaction à ces infractions.

\textbf{Dans le droit anglais}, droit de type anglo-saxon, la
distinction fondamentale s'effectue plutôt entre :
\begin{itemize}
\item la \emph{common law}, 
\item l'\emph{equity}, 
\end{itemize} \marginpar{Common law et Equity à creuser.}

Concernant l'objet de cette thèse, il était nécessaire de présenter
ces notions. En effet, le droit français comprend le \emph{droit
  d'auteur} qui est centré sur l'auteur comme son nom l'indique. Son
équivalent anglo-saxon est le \emph{copyright}, centré sur
l'imprimeur/éditeur, comme son nom l'indique également : Copyright
signifiant littéralement \emph{droit de copie}. Les licences libres
--- que nous étudierons dans quelques pages --- ont été créée au sein
d'un droit anglo-saxon.

\subsubsection{La nature du droit}

Tout travail de classification contient des incohérences. Cette
remarque apparait clairement après le travail des pages
précédentes. Et des interrogations concernant la véritable nature du
droit se posent.

\paragraph{Une fiction juridique}

En fait, nous avons révèlé le caractère fictionnel du droit : les
Hommes se racontent des histoires. C'est Rudolph \textsc{von Jhering}
(1818-1892), juriste allemand qui le premier parle de \emph{fiction
  juridique}. Cette notion n'a pas pour but de dévaloriser le droit :
selon \textsc{von Jhering}, le droit est « un mensonge technique
consacré par la nécessité ». \footnote{Voici quelques exemples de
  fictions juridiques qui peuvent nous éclairer sur cette notion :
\begin{itemize}
\item La \emph{personne morale} : on attribue une personnalité
  juridique --- et donc une naissance (fondation), une mort
  (dissolution), une identité (dénomination et adresse) ainsi que des
  droits et des devoirs --- à des regroupement d'individus qui
  souhaitent accomplir quelque chose ensemble : ce sont les
  associations, les entreprises, les États etc. L'autre type de
  personnalité juridique --- plus facile à concevoir --- est la
  \emph{personne physique} : un individu, un être humain. Remarquons
  que la personnalité juridique n'est pas forcément reconnue à tout
  être humain, comme c'est le cas dans les sociétés --- historiques
  voire actuelles --- qui pratiquent l'esclavage.
\item Le \emph{patrimoine}, qui est l'ensemble des droits et des
  obligations d'une personne juridique, notamment l'ensemble de ses
  droits de propriété.
\item Le \emph{fond de commerce} d'une entreprise,
\item l'\emph{adoption}.
\end{itemize}}

Ainsi, le droit est une œuvre de fiction collective ; et nous
remarquons avec amusement que cette œuvre est libre. Plus précisément,
chaque individu possède sur le droit les mêmes libertés que sur un
logiciel libre. En effet, chacun peut :
  \begin{itemize}
  \item \textbf{Liberté 0} : utiliser le droit.
  \item \textbf{Liberté 1} : étudier le droit et le modifier. La
    modification est évidemment soumise à quelques formalités avant
    d'être incluse dans la version officielle du droit ; mais c'est
    également le cas pour les logiciels libres. Par exemple, le droit
    de modifier la version officielle du droit comme celle d'un
    logiciel libre nécessite un niveau d'accréditation suffisant :
    être élu ou juge pour le droit, être \emph{committer} pour un
    logiciel. Et si une modification est refusée injustement, il y a
    toujours la possibilité de \emph{forker} le logiciel comme celle
    de faire cessession pour le droit. Il est vrai que ce type de
    manœuvre est encore moins favorablement accueillie dans le domaine
    du droit que dans celui de l'informatique par les défenseur de la
    version originale. Néanmoins, et toute proportion gardée, le droit
    en vigueur aux USA peut tout à fait être perçu comme un
    \emph{fork} du droit britannique.
  \item \textbf{Libertés 2 et 3} : diffuser le droit, dans sa forme
    originale, ou dans sa forme modifiée ; puisque \emph{nul n'est
      sensé ignorer la loi}.
  \end{itemize}

\paragraph{L'évolutivité du droit}

Le droit est également modifiable.

Nous avons vu que le droit définissait ce qui est autorisé et ce qui
est défendu ; il doit donc être distingué de l'\emph{éthique} dont le
but est de distinguer le bien du mal. Il semble néanmoins évident que
les rédacteurs du droit doivent s'attacher --- autant que possible ---
à rédiger un droit \emph{juste}, c'est-à-dire un droit qui autorise ce
qui est bon, et interdise ce qui est mal.

\paragraph{Les sources du droit}

Quels sont les auteurs du droit ? 

La constitution La loi Le règlement, par le pouvoir exécutif ou une
autorité administrative, pour préciser la loi, ou pour Le décret Le
contrat et la licence \marginpar{Étudier l'article sur la hiérarchie
  des normes en droit français.}

Un contrat se définit comme une convention formelle ou informelle,
passée entre deux parties ou davantage, ayant pour objet
l'établissement d'obligations à la charge ou au bénéfice de chacune de
ses parties. Le contrat est un acte juridique de droit privé, de la
famille des obligations, et de la catégorie des conventions. Une
convention est un accord de volonté conclu entre des personnes pour
créer, modifier, éteindre des obligations ou transférer des droits.

Le droit est modifiable. Il sera modifié de toute manière, dans un
sens comme de l'autre. Dans le sens de plus de liberté, ou dans le
sens de plus d'exclusivité pour chacun. C'est tout l'objet de la
querelle juridique en cours concernant les logiciels et les autres
œuvres de l'esprit.

\subsection{La propriété}

Nous devons maintenant nous attacher à définir les concepts de liberté
et de propriété car ces deux termes sont au centre des débats en
cours.

La \emph{propriété} est un \emph{privilège} concernant un \emph{bien}
qu'on accorde à une personne juridique.

Un privilège est un droit ou un ensemble de droits \emph{exclusifs},
c'est-à-dire, que ce ou ces droits ne concernent qu'une personne
juridique donnée, et pas d'autres ; l'expression \emph{à l'exclusion
  de tout autre personne} est souvent utilisée.

La propriété est constituée en fait de trois droits exclusifs
distincts : l'\emph{usus}, le \emph{fructus} et l'\emph{abusus} :
\footnote{Ces trois drois sont dits \emph{droits réels} car ils
  s'appliquent traditionnellement sur un objet physique. Seulement,
  cette chose peut également être immatérielle, notamment dans le cas
  de la \emph{propriété intellectuelle}.}
\begin{itemize}
\item \textbf{l'usus} est le droit d'utiliser un bien,
\item \textbf{le fructus} est le droit de profiter des conséquences
  d'un bien, littéralement celui \emph{d'en recueillir les fruits},
\item \textbf{l'abusus} est le droit de modifier un bien, de le céder
  à une autre personne juridique, voire de le détruire.
\end{itemize}

Nous voyons que la propriété ne se conçoit pas sans cette notion de
\emph{bien} : less entités sur lesquelles peuvent s'appliquer la
propriété. Cependant, la nature de ces entités est multiple, et on ne
possède pas de la même manière deux biens de nature
différente. Classons donc les biens pour mieux appréhender la notion
de propriété.

\subsubsection{La classification des biens selon leur nature}

Nous pouvons d'abord distinguer les biens selon leurs propriétés
physiques :

\begin{itemize}
\item \textbf{Les biens meubles et biens immobilier} du droit français
  distingue les biens selon la possibilité ou non à les déplacer. Un
  bien meuble --- déplaçable --- peut être \emph{corporel},
  c'est-à-dire avoir une existence physique, comme un animal, un
  meuble, un véhicule, ou être \emph{incorporel} \marginpar{Les biens
    meubles incorporels sont-ils des fictions juridiques ?}, donc ne
  pas avoir d'existence physique, comme un fond de commerce, comme les
  parts sociales d'une société, ou encore comme une œuvre littéraire
  ou artistique. Les biens non déplaçables tels qu'un terrain nu,
  qu'un bâtiment entier, ou qu'une partie d'un bâtiment, sont des
  biens immeubles, également appelés biens immobiliers.

\item \textbf{Les biens personnels et biens réels} du droit
  anglo-saxon. \marginpar{À creuser}

\item \textbf{Les biens matériels et biens immatériels}. Les biens
  matériels ont une existence physique --- les objets meubles ou
  immeubles --- et les biens immatériels --- les œuvres littéraires et
  artistiques, les données --- n'en ont pas. Avec l'invention de
  l'écriture puis de l'imprimerie puis de la radio et de la
  télévision, les biens immatériels n'ont fait que prendre de
  l'importance ---notamment économique --- pour la société. Avec les
  ordinateurs et Internet, nous entrons désormais dans une nouvelle
  ère, celle de la \emph{société de l'information} ou de la
  \emph{société de la connaissance}.

\item \textbf{Les biens rivaux et biens non-rivaux}. Cette distinction
  tient plus de l'économie que du droit. L'utilisation (ou la
  consommation) par un individu d'un bien rival est exclusive : elle
  empêche la consommation de ce bien par un autre individu. Alors que
  l'utilisation d'un bien non-rival est non exclusive. Ainsi, une
  technique thérapeutique, une formule chimique sont des biens
  non-rivaux ; un fond de commerce, une boîte de médicament sont des
  biens rivaux.
\end{itemize}

\subsubsection{La classification des biens selon la propriété}

Nous pouvons également distinguer quatres types de biens selon leur
propriété associée. Pour cela, le droit français reprend les
expressions latines du droit romain.
\begin{itemize}
\item \textbf{les res privatae} : les biens privés appartenant à une ou
  plusieurs personnes juridiques privées.
\item \textbf{les res publicae} : les biens publics appartenant à l'État,
\item \textbf{les res communes} : les \emph{biens communs} dont les
  droits d'usus et de fructus (mais pas d'abusus) sont accordés à
  tous. Ce sont classiquement des choses telles que les mers et océans
  ou encore l'atmosphère. Le \emph{domaine public} (sic) désigne en
  France l'ensemble de ces biens communs. Le \emph{patrimoine mondial
    de l'humanité} --- dont fait partie le système d'exploitation GNU
  depuis 2004 --- est une liste de biens dressée par l'UNESCO qui leur
  donne un statut de res communis. Les logiciels libres commencent à
  être désignés par certains auteurs sous le terme de \emph{biens
    communs numériques}.
\item \textbf{les res nullius} : les biens sans propriétaires --- ou
  \emph{biens sans maîtres} dans le droit français --- mais cependant
  appropriables. C'est le cas par exemple lorsque la personne
  propriétaire a disparu.
\end{itemize} \marginpar{Creuser le cas des enclosures.}

Cette classification présente des limites :
\begin{itemize}
\item Ainsi, les res communis peuvent être considérés comme
  n'appartenant à personne plutôt qu'à tout le monde.
\item Il est également juste de considérer l'État comme une personne
  juridique comme une autre, donc de considérer les biens publics
  comme des biens privés appartenant à l'État, et ainsi de considérer
  la propriété publique comme un simple cas particulier de propriété
  privée.
\end{itemize}


\subsubsection{Les limites de la propriété}

Nous devons insister sur le fait que la propriété n'est jamais un
droit absolu. Parmi les limites d'un droit de propriété, nous
connaissons bien :
\begin{itemize}
\item \textbf{la propriété partagée}, qui permet à plusieurs personnes
  juridiques de posséder ensemble un même bien, ou une
  \textbf{copropriété} qui divise un bien en plusieurs lots distincts
  répartis entre les propriétaires et en une ou des parties communes.
\item \textbf{la servitude}, qui stipule qu'un bien peut être utilisé
  par une autre personne juridique dans certains cas particuliers. La
  plus connue est certainement la \emph{servitude de passage} qui
  oblige un propriétaire à accorder un droit de passage sur son
  terrain à d'autres personnes, notamment parce que ces dernières ne
  pourraient pas accéder autrement à leur propre terrain.
\item \textbf{l'intérêt général} qui peut parfois prévaloir sur la
  propriété d'une personne juridique particulière. Par exemple, un
  propriétaire n'a pas le droit de déverser des produits chimiques sur
  son terrain, d'y cueillir ou de chasser des espèces de faune et de
  flore protégées, ni même d'y construire un gratte-ciel, sans
  l'accord des voisins et de la mairie,
\item \textbf{la dépossession} qui permet à l'État --- pour l'intérêt
  public --- de priver une personne propriétaire. Une
  \emph{expropriation} ou les \emph{prélèvements obligatoires} tels
  que les taxes et impôts sont des formes de dépossession.
\end{itemize}

Nous remarquons enfin que la propriété évolue avec le temps, et
notamment avec l'apparition de nouvelles technologie. Lawrence
\textsc{Lessig} prend l'exemple de la propriété privée aux USA :
classiquement, la propriété d'un terrain s'étendait vers le ciel
jusqu'à l'infini. Seulement, l'apparition de l'avion a bouleversé
cette vision. Et une décision judiciaire prise dans les années 40 a
finalement rejeté l'idée d'une possibilité de propriété privée du ciel
pour en faire catégoriquement un bien commun. \cite{Les04}

\subsection{La liberté}

Une définition de la liberté est qu'il s'agit de la faculté d'agir
selon sa volonté. Cette définition simple une notion philosophique
très complexe qui divisent les penseurs depuis
l'antiquité. \footnote{Philosophiquement, le clivage entre
  déterminisme --- Tout est écrit : la liberté n'est qu'une illusion
  face aux déterminismes eux bien réels--- et libre-choix, peut se
  déplacer entre physicalisme --- nos choix dits rationnels ne
  sont-ils finalement pas les résultats de nos propres sensations ?
  --- et mentalisme.} De même, politiquement et juridiquement,
l'application du concept de liberté est également très complexe.

La liberté se traduit juridiquement en plusieurs droits et devoirs :
\begin{itemize}
\item \textbf{Les libertés fondamentales} ou \textbf{libertés
    individuelles} qui doivent être garanties par l'État
  démocratique. Ce sont :
  \begin{itemize}
  \item le \emph{droit à la vie},
  \item la \emph{liberté de circulation} ou le droit d'aller et venir
    sans entrave au sein des frontières de l'État,
  \item la \emph{liberté de pensée} ou le droit d'avoir des pensées
    propres et donc des opinions,
  \item la \emph{liberté d'expression} ou le droit d'exprimer
    publiquement les pensées.
  \end{itemize}

\item \textbf{Des libertés individuelles réelles} découlent des
  libertés individuelles fondamentales : ce sont les droits à la
  santé, au logement, à la sécurité, au travail, à l'éducation, à la
  vie privée, ou encore le droit de vote.

\item \textbf{Des libertés collectives réelles} découlent également
  des libertés individuelles fondamentales. Elles sont :
  \begin{itemize}
  \item la \emph{liberté de réunion} ou le droit de se rassembler avec
    d'autres afin d'exprimer ses pensées,
  \item la \emph{liberté de la presse} ou le droit d'imprimer et
    diffuser ses pensées,
  \item le \emph{droit à l'autodétermination des peuples} ou
    \emph{droit des peuples à disposer d'eux-mêmes} qui autorise
    chaque peuple à déterminer hors de toute influence externe son
    régime politique et ses dirigeants. \footnote{Cependant,
      l'application de ce droit des peuples à l'autodétermination se
      heurte à la difficulté à définir précisément le concept de
      peuple, et donc à circonscrire un peuple par rapport au reste de
      l'Humanité.}
  \end{itemize}

\item Enfin --- et assez paradoxalement --- la liberté est source de
  \textbf{responsabilité} et de \textbf{devoirs}. En effet, tel
  individu a le devoir de respecter les droits de tel autre : « La
  liberté des uns s'arrête là où commence celle des autres ». La
  maxime de la carte électorale française « voter est un droit, c'est
  aussi un devoir civique » est également très éloquante.
\end{itemize}

Nous remarquons que la \emph{propriété} se traduit également en droits
subjectifs, en responsabilité et en devoirs ; la propriété privée est
en fait une liberté réelle.

\subsection{La propriété intellectuelle}

\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Les droits de propriété
    intellectuelle ne sont tous que des licences accordées par la
    société parce que nous pensions, à tort ou à raison, que la
    société en général bénéficierait de ces accords. Mais dans chaque
    situation précise, nous devons nous demander : avons-nous vraiment
    intérêt à accorder cette licence ? Quels actes autorisons-nous
    avec cette licence ?}}

\raggedleft Richard \textsc{Stallman}}
\vspace{2ex}

Dans l'antiquité, la liberté était avant tout perçue comme la
possibilité de ne pas avoir à travailler pour avoir le temps de
penser. En effet, le travail physique était alors perçu comme
alliénant --- il était imposé par autrui, ce qui était flagrant dans
le cas alors très courant de l'esclavage --- et dégradant --- le
travail enlaidissait manifestement le corps humain puisqu'il était
alors très physique et que les blessures inévitables n'était
probablement pas ou mal soignées.

Avec les religions du Livre qui apportent l'idée d'un Dieu libre
d'effectuer son propre travail, et la perspective d'une vie éternelle
de liberté en récompense d'une vie finie de labeur, travailler
physiquement devient libérateur. Et penser est même implicitement
perçu comme une activité oisive.

À la renaissance, s'effectue une diffusion accélérée des connaissances
et des idées via les livres imprimés. Avec les progrès de la Science ,
qui approfondit la connaissance, avec ceux de la médecine, qui donne
l'espoir aux humains d'avoir une vie plus longue et moins dépendantes
des maladies, avec ceux des techniques, qui soulagent considérablement
la charge de travail, le travail ne devient plus si repoussant. Mais
surtout, le penseur --- devenu auteur ou inventeur --- est rétabli. Sa
pensée, concrétisée grâce à un support physique : le livre, devient un
travail. Cela achève le clivage antique entre liberté de pensée et
obligation de travail. L'idée que les hommes puissent enfin être égaux
avec une vie équilibrée entre travail et liberté s'impose : l'égalité
des humains dans la liberté comme dans le travail, l'égalité des
humains dans les droits comme dans les devoirs.

Au vingtième siècle, avec les nouvelles technologies de l'information,
la connaissance et les idées prennent une forte importance
économique. Le penseur devient un travailleur comme un autre, et comme
tout artisan, il est propriétaire du fruit de son travail.

Une idée nouvelle peut entraîner un avantage pour la personne qui la
conçoit dans son esprit. Par exemple, un artisan peut concevoir une
nouvelle technique qui améliore sa production ou augmente sa
productivité ; il pourrait envisager qu'il est de son intérêt
particulier de garder cette idée pour lui afin de garder un avantage
sur ses confrères concurrents. Et c'était effectivement ce qui se
passait couramment, où les savoirs ne se transmettaient que de pères
en fils, puis qu'au sein d'une corporation.

Seulement l'intérêt général souffre de ces réflexes égoïste car cette
idée nouvelle pourrait améliorer la condition de tous les êtres
humains si elle était diffusée à tous les artisans qui pourrait la
mettre en pratique.

De plus, la diffusion d'une idée nouvelle peut stimuler l'imagination
d'autres individus qui vont alors concevoir de nouvelles idées qui
pourront à leur tour stimuler l'imagination de l'inventeur
premier. Ce cercle vertueux de progrès et d'accumulation des
connaissaissances dépend de la diffusion des idées.

C'est sur cette évolution des relations entre les concepts de travail
et de liberté de pensée que se fonde la propriété
intellectuelle. Celle-ci a pour but d'améliorer l'élaboration et la
diffusion des idées. Elle va ainsi inciter les auteurs et inventeurs à
publier, en leur garantissant certains droits exclusifs sur leurs
idées.

\subsubsection{La propriété littéraire et artistique}

La propriété littéraire et artistique s'applique aux œuvres
littéraires et artistiques. Elle est composée par :
\begin{itemize}
\item le droit d'auteur
\item les droits voisins
\item le copyright.
\end{itemize}

Le droit d'auteur est pensé initialement comme un droit d'équilibre
entre les auteurs, le public et les intermédiaires. Il fallait
protéger les auteurs contre les intermédiaires, alors qu'aujourd'hui
il agit comme une protection des intermédiaires contre le public.

Les droits voisins ont été calqués sur le droit d'auteur. Ils
s'appliquent aux interprétations, 

\subsubsection{La propriété industrielle}

\paragraph{Les brevets et les patents}

Le droit des brevets a été conçu dans le but d'inciter les gens à
diffuser les idées. Mais aujourd'hui, ce but n'est plus qu'une excuse.

Un brevet donne des droits exclusifs sur une idée. Un programme
combine des milliers d'idées.  Dans d'autres domaines, les brevets
n'ont pas le même. Il n'y a pas de centaines de brevets sur une même
drogue.

Le problème des brevets sur les médicaments ne pose pas vraiment de
problèmes sur l'inovation des médicament, mais pose des problèmes pour
les pays du tiers-monde. Il y a quelques années en Inde, il y a eu une
loi pour brevet les méthodes.

Pour l'agriculture, les brevets nient aux agriculteurs le droits de
reproduire les semences.

\paragraph{Le droit des marques et les trademarks}

\subsubsection{Les idées qui échappent à la propriété intellectuelle}

\paragraph{Les arts non soumis à la propriété intellectuelle}

La science. En fait, la science a réussi à concilier les deux.

La mode

La cuisine

Les ADN

\paragraph{Le domaine public}

\marginpar{Windows tombera t-il un jour dans le domaine public ?}

\url{http://www.publicdomainmanifesto.org/french}

\url{http://www.framablog.org/index.php/post/2013/01/19/non-privatisation-domaine-public-bnf}

\subsection{Un équilibre en faveur de l'intérêt général}

Le texte de la déclaration des droits humains stipule dans l'article
27 :
\begin{enumerate}
\item Toute personne a le droit de prendre part librement à la vie
  culturelle de la communauté, de jouir des arts et de participer au
  progrès scientifique et aux bienfaits qui en résultent.
\item Chacun a droit à la protection des intérêts moraux et matériels
  découlant de toute production scientifique, littéraire ou artistique
  dont il est l'auteur.
\end{enumerate}

L'article 27 semble se contredire lui-même.

\section{Le droit du Libre}
\label{sec:dl}

\vspace{4ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Je crois même que c'est la
      marque d'un petit esprit, incapable de rattacher le présent au
      passé, que de se croire inventeur de principes. Les sciences ont
      une croissance comme les plantes ; elles s'étendent, s'élèvent,
      s'épurent. Mais quel successeur ne doit rien à ses devanciers
      ?}}

  \raggedleft Frédéric \textsc{Bastiat}}
\vspace{4ex}

Nous avons vu que la propriété intellectuelle se voulait en fait un
équilibre entre divers intérêts particuliers --- ceux des
auteurs/inventeurs, ceux des intermédiaires, ceux du public --- d'une
part pour faire émerger l'intérêt général d'autre part. Or des
insuffisances dans ces droits de propriété intellectuelle se font jour
et conduisent à une remise en cause profonde du concept
(\ref{sec:remisecause}) et les licences libres proposent une solution
concrète pour établir un nouvel équilibre. (\ref{sec:licences})

\section{Le libre conteste t-il la propriété privée ?}

\vspace{4ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Celui qui a inventé la
      charrue laboure encore, invisible, à côté du laboureur. [...]
      Qu'avons nous donc qui nous appartienne absolument en propre et
      en entier, au point de vue rigoureux de la science pure ? Bien
      peu de chose. [...] La société est un véritable organisme dont
      nous sommes les cellules vivantes.}}

\raggedleft Alfred \textsc{Fouillée}}
\vspace{4ex}

En fait, le libre ne conteste généralement pas la notion de paternité
des idées.

Nous décrivons ici une nouvelle façon de considérer la création. Le
libre la considère comme collaborative et évolutive. Nous sommes loin
de la représentation dite \emph{classique} de l'auteur solitaire
apportant la création à un public la consommant passivement. Et nous
postulons que le Libre a raison.

Que serait la musique aujourd'hui si le premier musicien ayant utilisé
la gamme de do majeur avait déposé un brevet la concernant, si ce
brevet avait une durée illimité, ou qu'il fallait demander une
autorisation ou payer une licence au moindre acte humain ? La
propriété intellectuelle --- pour autant qu'il y en est une --- doit
donc avoir des limites elle aussi.

Le bon sens devrait suffire. Car la liberté des uns s'arrête là où
commence celle des autres. Et la liberté de gagner de l'argent avec
son œuvre s'arrète donc là où il faudrait écorner le droit à la vie
privée du public pour le faire respecter.

Mais le bon sens ne semble pas suffire, et le Libre doit affronter des
détracteurs.

Eben \textsc{Moglen}, professeur de droit à l'université Columbia,
président du Software Freedom Law Center, et fin provocateur \footnote
{Il faut savoir que le communisme est un sujet historiquement sensible
  aux États-Unis, encore plus qu'ailleurs.}, a publié en 2003 une
version libriste du manifeste du parti communiste de Karl
\textsc{Marx} et Friedrich \textsc{Engels}. Bill Gates, fondateur de
Microsoft explique en 2005 que les États-Unis seraient la proie d'une
nouvelle sorte de communisme. En guise de réponse, Lawrence
\textsc{Lessig}, précise qu'il ne s'agit pas de communisme mais de
commonisme, c'est-à-dire de défense des biens communs.

Ainsi une question --- bien plus politique que juridique --- semble se
poser : le libre est-il un communisme ? \footnote{Il faut constater
  que la question est devenu un troll poilu qui sévit régulièrement
  sur les forums, à l'instar du débat qui divise les adorateurs de Vi
  et ceux d'Emacs depuis des décennies. Ce dernier débat est
  évidemment stérile, tant Emacs surclasse Vi.}

Le libre --- idéologie résolument moderne --- apporte pourtant la
réponse aux vielles querelles et une fin aux meurtres et aux guerres
perpétrés en leurs noms en proclamant :
\begin{itemize}
\item propriété privée pour l'objet, c'est-à-dire le bien matériel,
  corporel, rival.
\item propriété commune pour l'idée, c'est-à-dire le bien immatériel,
  incorporel, non-rival.
\end{itemize}

Il existe néanmoins un cas d'entité immatériel qui semble contredire,
cette distinction simpliste : il s'agit des données concernant la vie
privée. En effet, selon la plupart des libristes \footnote{à
  l'exception notable de Laurent \textsc{Chemla} qui explique que les
  petits secrets du commun des mortels sont négligeables et que les
  seuls à avoir réellement à perdre par l'abolition de la vie privée
  sont les puissants.} \marginpar{Retrouver les dires exactes de
  Chemla} les données relatives à la vie privée, pourtant de nature
immatérielle, doivent rester la propriété privée de l'individu
concerné.

Mais un simple parallèle avec la propriété privée des biens matériels
suffit à résoudre le dilemme. En effet, il existe des exceptions
flagrantes à la propriété privée, telles que l'esclavage. Bien
qu'heureusement aboli aujourd'hui, cet exemple prouve que le fait
qu'un être humain n'entre pas dans la catégorie des biens bien qu'il
soit quelquechose de corporel, matériel, ne va pas forcément de
soi. Il a donc fallu modifier le droit de ses sociétés pour abolir
l'esclavage. Ainsi, puisqu'il existe des exceptions flagrantes à la
propriété privée des biens matériels, il semble évidemment possible
d'en faire concernant la propriété commune des biens immatériels. Et
si un humain ne peut et ne dois, selon le droit, jamais être considéré
comme un bien, alors une donnée personnelle ne devraient jamais, selon
le droit, être considérée comme un bien non plus.

De plus, nous constatons que le Libre s'inscrit complétement dans la
pensée économique libérale, contre laquelle il semble \footnote{Selon
  les slogans politiques entendus en France depuis des décennies} que
le communisme s'érige. Ce que ne contredit pas Richard
\textsc{Stallman} qui s'exprime très clairement : \marginpar{Retrouver
  la citation : l'économie du logiciel libre est une économie libre
  dans un marché libre.}

Mais nous devrions étudier plus à fond l'économie --- comme nous
venons de le faire avec le droit --- et l'économie du Libre, afin
d'appréhender cette question.