 
 \chapter[Introduction]{Introduction :\\ une thèse en pharmacie\\ sur
   les logiciels libres}

  \parbox{0.6\textwidth}{{\sffamily {\itshape Chaque génération a son
      philosophe, écrivain ou artiste qui saisit et incarne
      l'imaginaire du moment. Il arrive que ces philosophes soient
      reconnus de leur vivant, mais le plus souvent il faut attendre
      que la patine du temps fasse son effet. [...] Notre génération a
      un philosophe. Ce n'est ni un artiste ni un écrivain. C'est un
      informaticien.}} \footnotemark

  \raggedleft Lawrence \textsc{Lessig}}

\footnotetext{Cette citation provient de la préface par Lawrence
  \textsc{Lessig} du recueil des essais de Richard \textsc{Stallman}
  intitulé \emph{Free Software, Free Society}. L'ensemble de ce
  recueil n'a pas fait l'objet d'une traduction française, cependant,
  nombre de ces essais sont lisibles en français sur le site de
  GNU. \cite{Gnu}} \vspace{4ex}

Vers 1450, Gutenberg avait inventé une machine capable de produire de
nombreux livres en beaucoup moins de temps que ne le faisaient les
copistes de l'époque : la presse à imprimer. Tous ces livres
participèrent à l'émergence de nouvelles idées qui révolutionnèrent la
société : la \emph{philosophie des lumières}. Les conditions dont nous
profitons de nos jours en France --- telles que le confort de vie, la
médecine scientifique, mais aussi la démocratie --- découlent de
l'imprimerie.

Vers le milieu du vingtième siècle, une invention analogue à celle de
l'imprimerie est apparue : l'\emph{ordinateur}. Les ordinateurs sont
capables --- grâce à \emph{Internet}, le réseau reliant tous les
ordinateurs entre eux --- de diffuser des milliers de copies
numériques d'un livre dans le monde entier de façon presque
instantanée pour un coût marginal \footnote{Le coût marginal est le
  coût théorique induit par la production d'une unité supplémentaire
  d'un lot. \cite{Wikipedia}} nul. Avec de telles possibilités de
diffusion des connaissance, c'est une nouvelle révolution qui
émerge. Cette révolution se nomme le \emph{logiciel libre}. Ou le
\emph{Libre} de façon plus générale.

Que cette révolution émerge parmi les informaticiens ne doit pas nous
surprendre. Car ce sont eux qui ont accès aux ordinateurs et qui
\emph{savent} les utiliser. Et déjà, les philosophes des lumières
comptaient parmi ceux de leur contemporains qui avaient accès aux
livres et qui possédaient les \emph{savoir-faire} nécessaires pour
les utiliser : la lecture et l'écriture.

Que cette révolution ne soit aujourd'hui perçue comme telle que par
très peu de gens ne doit pas non plus nous surprendre. Car peu d'entre
nous possèdent en fait la culture et les savoir-faire adéquats. De
même, la très grande majorité des contemporains de
\textsc{Montesquieu} ou de \textsc{Lavoisier} ne se sentaient pas eux
non plus concernés par leurs écrits, puisqu'ils ne savaient tout
simplement pas les lire.

De là, l'idée que cette nouvelle révolution ne puisse être profitable
à tous qu'à la condition que chacun dispose des connaissances et des
compétences nécessaires, ne devrait pas surprendre elle non plus. Car
à une époque, il avait aussi fallu créer l'école --- gratuite, laïque
et obligatoire --- pour inculquer la lecture, l'écriture et une
culture générale à toute la population pour que chacun exerce enfin
ses droits et ses devoirs civiques.

\textbf{Nous nous proposons donc d'explorer l'impact d'une telle
  révolution dans le domaine de la santé.}

\section*{Objectifs}

Cette thèse suit plusieurs objectifs :
\begin{enumerate}
\item \textbf{Présenter aux lecteurs les notions de Libre et de
    logiciel libre.} Ces notions sont actuellement mal comprises par
  l'ensemble des professionnels et étudiants en santé alors qu'elles
  auront un impact --- positif --- considérable sur leurs métiers.
\item \textbf{Proposer des pistes d'applications du Libre au monde de
  la santé.}
\end{enumerate}

Aujourd'hui, les professionnels de santé ont inclus les ordinateurs
dans la pratique quotidienne de leur métier. Cependant, cet outil de
travail omniprésent reste largement méconnu. Car les professionnels et
étudiants en santé n'en connaissent bien souvent que la surface. Et il
en résulte le sentiment diffus de subir les évolutions de ces métiers.

C'est en fait dès leurs études que ceux-ci doivent prendre le temps de
choisir soigneusement les logiciels qui les aideront à prendre des
notes, effectuer des recherches documentaires et rédiger des
rapports. Quelques-uns iront plus loin et rechercheront des logiciels
pour effectuer une comptabilité, créer des affiches, paginer un
journal associatif, ou encore mettre en ligne un site web.

Durant cette quête des outils les plus performants, ils dénicheront à
coup sûr des \emph{logiciels libres}. Et certains, intrigués par
l'alliance apparemment paradoxale de la qualité et de la gratuité chez
un même logiciel mèneront des recherches sur cette notion de
\emph{Libre}. 

Enfin, ils finiront par se convaincre que tout logiciel devrait être
libre. Ils prendront alors la décision d'installer sur leurs
ordinateurs personnels un système d'exploitation libre\footnote{Pour
  une première installation, nous conseillons l'une ou l'autre des
  plus populaires distributions de \emph{GNU/Linux} --- telle que
  l'incontournable \emph{Debian} --- bien qu'elles contiennent encore
  quelques logiciels non-libres. À terme, nous préconisons une seconde
  migration vers une distribution totalement libre comme
  \emph{Parabola GNU/Linux-libre}, version d'\emph{ArchLinux}
  débarrassée du moindre composant non-libre. C'est par exemple
  Parabola que nous avons utilisée pour l'intégralité du travail
  effectué pour cette thèse.}

\textbf{En 2014 --- et cette thèse le prouve --- tous les besoins
  informatiques d'un étudiant peuvent être satisfaits par le logiciel
  libre.} Seulement, nous faisons le constat d'un manque de visibilité
des solutions informatiques professionnelles libres en santé, quand ce
n'est pas un manque tout court pour de nombreuses professions, dont la
pharmacie.

En effet, un progiciel\footnote{Progiciel est un mot-valise qui
  résulte de la fusion de \emph{logiciel} et de \emph{professionnel} ;
  tout comme \emph{informatique} qui allie \emph{information} avec
  \emph{automatique}.} de santé \emph{libre} doit présenter les
caractéristiques suivantes :
\begin{itemize}
\item \textbf{être accessible à tous} sans coût de licence
  d'utilisation ;
\item \textbf{être exécutable pour tout usage}, professionnel ou non,
  commercial ou non ; aussi bien, par exemple, pour la pratique
  professionnelle que pour l'apprentissage durant les études ;
\item \textbf{être conçu dans l'intérêt des utilisateurs}, donc dans
  celui des professionnels de santé et de leurs patients, pas dans
  celui d'une maison d'édition de logiciels ;
\item \textbf{être développé collaborativement} par toutes les
  personnes qui se sentent concernées : des bénévoles isolés aussi
  bien que des employés d'organisations diverses et variées
  (établissements d'enseignement supérieur, entreprises de services
  informatiques, ordres professionnels, groupements de praticiens,
  syndicats).
\end{itemize}

Nous pouvons donc déjà imaginer que le manque de tels logiciels pour
les professions de santé est un problème qui va bien au delà du simple
choix de la marque d'un outil. C'est ce que nous tenterons de
démontrer dans ces pages. Et nous espérons qu'avec les notions
introduites ici, les professionnels de santé comprendront qu'ils ont
besoin de logiciels libres, et du Libre par extension.

\section*{Plan}

Cette thèse est divisée en quatre chapitres qui se succèdent dans le
temps tout en se chevauchant partiellement.
\begin{itemize}
\item Nous analysons d'abord la notion de Libre :

  \begin{itemize}
  \item Dans le \textbf{chapitre \ref{chap:info} page
      \pageref{chap:info}}, nous posons les bases d'informatique
    nécessaires.
  \item Dans le \textbf{chapitre \ref{chap:logi} page
      \pageref{chap:logi}}, nous découvrons la façon dont le
    \emph{Libre} s'est développé dans ce domaine de l'informatique
    pour donner le logiciel libre.
  \item Dans le \textbf{chapitre \ref{chap:libre} page
      \pageref{chap:libre}}, nous présentons les transformations
    influencées par le Libre dans des domaines très variés (les
    connaissances, et donc l'enseignement, mais aussi l'art et la
    politique).
  \end{itemize}

\item Tout cela afin de démontrer qu'il est inéluctable que le Libre
  pénètre enfin dans le secteur de la santé. C'est ce que nous
  présentons dans le \textbf{chapitre \ref{chap:sante} page
    \pageref{chap:sante}}.
\item Enfin, en \textbf{conclusion page \pageref{chap:conc}}, nous
  proposons de nombreuses pistes étendant notre sujet du Libre en
  santé.
\end{itemize}

\section*{Recherche}

Voici donc les trois axes que nous avons suivi durant nos recherches :
\begin{enumerate}
\item \textbf{L'informatique}. La rédaction de cette thèse a nécessité
  au préalable l'apprentissage de nombreux logiciels libres, ainsi que
  le suivi régulier de leur actualité. Les notions nécessaires à leur
  installation et leur utilisation sont librement disponibles sur le
  web --- bien que disséminées. Nous remercions l'ensemble de tous les
  contributeurs qui rédigent et/ou traduisent ces documentations.
\item \textbf{Les sciences humaines}. Le logiciel libre relève
  paradoxalement peu de l'informatique. Il s'agit d'abord d'une notion
  juridique, économique, sociologique et donc politique. Le droit et
  l'économie font heureusement partie des matières enseignées dans les
  facultés de pharmacie. Cependant, il nous a été nécessaire
  d'approfondir certaines notions de ces disciplines. Pour cela et
  pour bien d'autres besoins, Wikipedia fut d'une aide
  inestimable\footnote{Wikipedia \cite{Wikipedia} est d'ailleurs
    l'exemple parfait de la connaissance libre. Son fonctionnement
    repose évidemment sur un logiciel libre, \emph{Mediawiki} :
    \url{http://www.mediawiki.org/}.}.  Wikipedia et toutes les
  sources utilisées dans nos recherches sont cités dans la
  bibliographie.
\item \textbf{L'actualité du secteur de la santé}. Réformes
  gouvernementales inadaptées, dépendance croissante des professionnels
  de santé de proximité, scandales sanitaires --- avérés ou non ---
  les menaces pèsent actuellement sur un secteur de la santé
  résigné. Comme nous l'expliquerons dans cette thèse, le logiciel
  libre est la preuve que notre époque est également porteuse
  d'opportunités à saisir.
\end{enumerate}

\section*{Rédaction}

\textbf{La rédaction de cette thèse a été réalisée exclusivement à l'aide de
logiciels libres, en particulier :}

\begin{itemize}
\item le système de composition de texte \emph{\TeX} et son ensemble
  de macros \emph{\LaTeX}\footnote{Nous recommandons l'ouvrage de
    Vincent \textsc{Lozano} \emph{Tout ce que vous avez toujours voulu
      savoir sur \LaTeX \enspace sans jamais osé le
      demander}. \cite{Loz08}},
\item l'éditeur de texte \emph{GNU Emacs} et notamment son mode
  \emph{AUC/TeX},
\item le logiciel de graphisme vectoriel \emph{Inkscape} pour les
  schémas,
\item les logiciels de traitement d'image \emph{Gimp} et
  \emph{ImageMagick} pour la retouche des photographies,
\item le gestionnaire de révision \emph{Git},
\item ainsi que l'ensemble des programmes libres constituant le
  système d'exploitation \emph{GNU/Linux}..\footnote{Nous recommandons
    l'utilisation --- au moins partielle --- des interfaces
    textuelles. Pour leur apprentissage, nous conseillons :
  \begin{itemize}
  \item le guide \emph{Ligne de commande} des éditions Diamond,
    \cite{Dia13}
  \item \emph{Pour aller plus loin avec la ligne de commande} de
    Vincent \textsc{Lozano}, \cite{Loz10}
  \item \emph{UNIX} de Dave \textsc{Taylor} et James
    C. \textsc{Armstrong} Jr. \cite{Tay04}
  \end{itemize}}
\end{itemize}

Nous remercions les personnes qui développent et maintiennent les
programmes suscités. Mais à travers eux, c'est finalement l'ensemble
des hackers qui contribuent depuis trente ans à l'avancée du logiciel
libre que nous remercions.

\begin{flushright}
\vspace{2ex}
\parbox{0.6\textwidth}{{\sffamily {\itshape Si on ne rentre pas dans
      le détail, ils ne nous croiront pas ; et si on rentre dans les
      détails, ils ne nous comprendront pas.}}

\raggedleft Roberto \textsc{Di Cosmo}}
\end{flushright}